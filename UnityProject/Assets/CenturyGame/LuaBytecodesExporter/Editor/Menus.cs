﻿/**************************************************************
 *  类名称：          Menus
 *  描述：
 *  作者：            Chico(wuyuanbing)
 *  创建时间：        2020/10/26 14:43:54
 *  最后修改人：
 *  版权所有 （C）:   CenturyGames
 **************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CenturyGame.LuaBytecodesExporter.Runtime;
using UnityEditor;
using UnityEngine;

namespace CenturyGame.LuaBytecodesExporter.Editor
{
    public class Menus
    {
        static string OptimazePath(string path, bool getFullPath = true)
        {
            if (getFullPath)
                path = Path.GetFullPath(path);
            path = path.Replace(@"\", @"/");
            return path;
        }

        static string TargetExportConfigRelativePath = "Assets/CenturyGamePackageRes/LuaBytecodesExporter/ExportConfig.asset";

        public static LuaByteCodesExportConfig GetLuaByteCodesExportConfig()
        {
            var config = AssetDatabase.LoadAssetAtPath<LuaByteCodesExportConfig>(TargetExportConfigRelativePath);
            return config;
        }

        [MenuItem("CenturyGame/LuaBytecodesExporter/创建Lua字节码导出配置")]
        static void Create()
        {
            var relativePath = TargetExportConfigRelativePath;
            var path = $"{Application.dataPath}/../{relativePath}";
            path = Path.GetFullPath(path);
            path = path.Replace(@"\", "/");

            bool create = true;
            if (File.Exists(path))
            {
                if (!EditorUtility.DisplayDialog("创建Lua字节码导出配置", "是否覆盖现有的配置？", "OK", "Cancle"))
                {
                    create = false;
                }
            }

            if (create)
            {
                var appSetting = ScriptableObject.CreateInstance<Runtime.LuaByteCodesExportConfig>();
                var dir = Path.GetDirectoryName(path);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                AssetDatabase.CreateAsset(appSetting, relativePath);
            }
            else
            {
                Debug.LogWarning("You cancled to create the default lua bytecodes config! ");
            }
        }

        [MenuItem("CenturyGame/LuaBytecodesExporter/导出字节码")]
        public static void ExportByteCodes()
        {
            var config = GetLuaByteCodesExportConfig();

            if (config == null)
            {
                throw new FileNotFoundException("Lua字节码导出器配置没找到！");
            }

            var pythonScripPath = $"{Application.dataPath}/../{config.pythonScriptPath}";
            pythonScripPath = OptimazePath(pythonScripPath);
            if (!File.Exists(pythonScripPath))
                throw new FileNotFoundException("Python 工具脚本未找到");

            var luaSourcePath = $"{Application.dataPath}/../{config.sourceFoler}";
            luaSourcePath = OptimazePath(luaSourcePath);

            Debug.Log($"luaSourcePath : {luaSourcePath}.");
           

            var luaTargetFolderPath = $"{Application.dataPath}/../{config.outputFolder}";
            luaTargetFolderPath = OptimazePath(luaTargetFolderPath);
            Debug.Log($"luaTargetFolderPath : {luaTargetFolderPath}.");

            Exporter.ExportByteCodes(pythonScripPath, luaSourcePath, luaTargetFolderPath,config.stripDebugInfo);
        }
    }
}