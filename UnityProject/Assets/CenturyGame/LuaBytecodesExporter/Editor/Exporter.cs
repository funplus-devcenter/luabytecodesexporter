﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;
using UnityEditor;
using Debug = UnityEngine.Debug;

internal class Exporter
{
    static string OptimazePath(string path, bool getFullPath = true)
    {
        if (getFullPath)
            path = Path.GetFullPath(path);
        path = path.Replace(@"\", @"/");
        return path;
    }

    public static void ExportByteCodes(string pythonScripPath, string luaSourcePath , string luaTargetFolderPath , bool stripDebugInfo)
    {
        string luaRootPath = luaSourcePath;
        Debug.Log($"Lua projecet root path : {luaRootPath} .");
        pythonScripPath = OptimazePath(pythonScripPath);
        string commandLineArgs = null;
        if(stripDebugInfo)
            commandLineArgs = $"{pythonScripPath} {luaRootPath} {luaTargetFolderPath} -s";
        else
            commandLineArgs = $"{pythonScripPath} {luaRootPath} {luaTargetFolderPath}";
        Debug.Log($"commandline args : {commandLineArgs}");

        var pStartInfo = new ProcessStartInfo();

#if UNITY_EDITOR_WIN
        pStartInfo.FileName = @"python.exe";
#elif UNITY_EDITOR_OSX
            pStartInfo.FileName = @"python";
#else
        throw new InvalidOperationException($"Unsupport build platform : {EditorUserBuildSettings.activeBuildTarget} .");
#endif


        pStartInfo.UseShellExecute = false;

        pStartInfo.RedirectStandardInput = false;
        pStartInfo.RedirectStandardOutput = true;
        pStartInfo.RedirectStandardError = true;
        var workDir = Path.GetDirectoryName(pythonScripPath);
        workDir = OptimazePath(workDir);
        pStartInfo.WorkingDirectory = workDir;

        pStartInfo.CreateNoWindow = true;
        pStartInfo.WindowStyle = ProcessWindowStyle.Normal;
        pStartInfo.Arguments = commandLineArgs;

        pStartInfo.StandardErrorEncoding = System.Text.UTF8Encoding.UTF8;
        pStartInfo.StandardOutputEncoding = System.Text.UTF8Encoding.UTF8;

        var proces = Process.Start(pStartInfo);
        string standardOutput = proces.StandardOutput.ReadToEnd();
        if (!string.IsNullOrWhiteSpace(standardOutput))
            UnityEngine.Debug.Log(standardOutput);

        string standardErroOutput = proces.StandardError.ReadToEnd();
        if (!string.IsNullOrWhiteSpace(standardErroOutput))
            UnityEngine.Debug.LogError(standardErroOutput);

        proces.WaitForExit();
        proces.Close();
        Debug.Log("Export lua byte codes success!");

        AssetDatabase.Refresh();
    }

}
