﻿/**************************************************************
 *  类名称：          LuaByteCodesExportConfig
 *  描述：            Lua字节码导出工具
 *  作者：            Chico(wuyuanbing)
 *  创建时间：        2020/10/26 14:34:58
 *  最后修改人：
 *  版权所有 （C）:   CenturyGames
 **************************************************************/

using UnityEngine;

namespace CenturyGame.LuaBytecodesExporter.Runtime
{
    public class LuaByteCodesExportConfig : ScriptableObject
    {
        /// <summary>
        /// Python工具脚本路径
        /// </summary>
        [Header("Python工具脚本路径（相对于工程目录）")]
        public string pythonScriptPath = "";

        /// <summary>
        /// Lua源目录
        /// </summary>
        [Header("Lua 源代码根目录 （相对于工程目录）")]
        public string sourceFoler = "LuaProject ";

        /// <summary>
        /// Lua字节码路径
        /// </summary>
        [Header("Lua 字节码根目录 （相对于工程目录）")]
        public string outputFolder = "LuaByteCodesProject";

        [Header("是否裁剪字节码的调试信息")]
        public bool stripDebugInfo = false;

    }
}