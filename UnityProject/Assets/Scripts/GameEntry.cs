﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using XLua;

public class GameEntry : MonoBehaviour
{

    LuaEnv mEnv;


    public bool use_bytecodes = false;
    // Start is called before the first frame update
    void Start()
    {
        mEnv = new LuaEnv();

        this.mEnv.AddLoader((ref string s) =>
        {
            if (use_bytecodes)
                return null;
            string filename = $"{Application.dataPath}/../LuaProject/{s}.lua";

            var bytes = File.ReadAllBytes(filename);

            return bytes;
        });

        this.mEnv.AddLoader((ref string s) =>
        {
            string filename = $"{Application.dataPath}/../LuaByteCodesProject/{s}.lua";

            var bytes = File.ReadAllBytes(filename);

            return bytes;
        });
    }

    // Update is called once per frame
    void Update()
    {
        mEnv?.Tick();
    }

    private void OnDestroy()
    {
        mEnv.Dispose();
        mEnv = null;
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Do operation by lua source code"))
        {
            this.mEnv.DoString("require 'main'");
        }

        if (GUILayout.Button("Do operation by lua byte code"))
        {
            use_bytecodes = true;
            this.mEnv.DoString("require 'main_lbc'");
        }
    }
}
