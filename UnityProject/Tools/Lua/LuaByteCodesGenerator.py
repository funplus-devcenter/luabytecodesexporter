import pathlib
import subprocess
import os
import sys
import shutil
import platform

SOURCE_PATH = None
OUTPUT_PATH = None
STRIP_DEBUG_INFO = None

def exportLB(source_file_path,targetName):
    luac_p = pathlib.Path(os.getcwd())
    cmd = None
    if platform.system() == 'Windows':
        if STRIP_DEBUG_INFO :
            cmd = ['%s/luac.exe' % luac_p.as_posix(), '-s', '-o', '%s' % targetName, '%s' % source_file_path]
        else:
            cmd = ['%s/luac.exe' % luac_p.as_posix(),'-o', '%s' % targetName, '%s' % source_file_path]
    else:
        if STRIP_DEBUG_INFO:
            cmd = ['%s/luac' % luac_p.as_posix(), '-s', '-o', '%s' % targetName, '%s' % source_file_path]
        else:
            cmd = ['%s/luac' % luac_p.as_posix(), '-o', '%s' % targetName, '%s' % source_file_path]

    print(cmd)
    subprocess.run(cmd)

def generateLB(f):
    if f.is_file():
        path_args = os.path.split(f.as_posix())
        name = path_args[1]
        if name.endswith('.lua'):
            reletive_path = f.as_posix().replace(SOURCE_PATH.as_posix(),'')
            new_path = OUTPUT_PATH.as_posix() + reletive_path
            new_dir = os.path.split(new_path)[0]
            if not os.path.exists(new_dir):
                os.makedirs(new_dir)
            print(new_path)
            exportLB(f.as_posix(),new_path)
    else:
        for fi in f.iterdir():
            generateLB(fi)

def GenerateLuaBytes():
    for f in SOURCE_PATH.iterdir():
        generateLB(f)

if __name__ == '__main__':
    print('Commandline args length is : %u '%len(sys.argv))
    if len(sys.argv) < 3:
        raise ValueError
    lua_source_path = sys.argv[1].strip()
    out_put_folder_path = sys.argv[2].strip()
    if len(sys.argv) == 4:
        strip_debug_flag = sys.argv[3]
        if strip_debug_flag == '-s':
            STRIP_DEBUG_INFO = True

    SOURCE_PATH = pathlib.Path(lua_source_path)
    OUTPUT_PATH = pathlib.Path(out_put_folder_path)
    print('source : %s .'%SOURCE_PATH.as_posix())
    print('output : %s .'%OUTPUT_PATH.as_posix())
    if os.path.exists(OUTPUT_PATH):
        shutil.rmtree(OUTPUT_PATH,ignore_errors=True)
    os.mkdir(OUTPUT_PATH.as_posix())
    GenerateLuaBytes()
