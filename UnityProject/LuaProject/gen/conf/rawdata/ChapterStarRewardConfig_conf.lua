return {
	[101001] = {
		["ID"] = 101001,
		["Name"] = "奖励1",
		["ChapterID"] = 101,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[101002] = {
		["ID"] = 101002,
		["Name"] = "奖励2",
		["ChapterID"] = 101,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[101003] = {
		["ID"] = 101003,
		["Name"] = "奖励3",
		["ChapterID"] = 101,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[101004] = {
		["ID"] = 101004,
		["Name"] = "奖励1",
		["ChapterID"] = 101,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[101005] = {
		["ID"] = 101005,
		["Name"] = "奖励2",
		["ChapterID"] = 101,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102001] = {
		["ID"] = 102001,
		["Name"] = "奖励3",
		["ChapterID"] = 102,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102002] = {
		["ID"] = 102002,
		["Name"] = "奖励1",
		["ChapterID"] = 102,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102003] = {
		["ID"] = 102003,
		["Name"] = "奖励2",
		["ChapterID"] = 102,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102004] = {
		["ID"] = 102004,
		["Name"] = "奖励3",
		["ChapterID"] = 102,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102005] = {
		["ID"] = 102005,
		["Name"] = "奖励1",
		["ChapterID"] = 102,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102006] = {
		["ID"] = 102006,
		["Name"] = "奖励2",
		["ChapterID"] = 102,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102007] = {
		["ID"] = 102007,
		["Name"] = "奖励3",
		["ChapterID"] = 102,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102008] = {
		["ID"] = 102008,
		["Name"] = "奖励1",
		["ChapterID"] = 102,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102009] = {
		["ID"] = 102009,
		["Name"] = "奖励2",
		["ChapterID"] = 102,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[102010] = {
		["ID"] = 102010,
		["Name"] = "奖励3",
		["ChapterID"] = 102,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103001] = {
		["ID"] = 103001,
		["Name"] = "奖励1",
		["ChapterID"] = 103,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103002] = {
		["ID"] = 103002,
		["Name"] = "奖励2",
		["ChapterID"] = 103,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103003] = {
		["ID"] = 103003,
		["Name"] = "奖励3",
		["ChapterID"] = 103,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103004] = {
		["ID"] = 103004,
		["Name"] = "奖励1",
		["ChapterID"] = 103,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103005] = {
		["ID"] = 103005,
		["Name"] = "奖励2",
		["ChapterID"] = 103,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103006] = {
		["ID"] = 103006,
		["Name"] = "奖励3",
		["ChapterID"] = 103,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103007] = {
		["ID"] = 103007,
		["Name"] = "奖励1",
		["ChapterID"] = 103,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103008] = {
		["ID"] = 103008,
		["Name"] = "奖励2",
		["ChapterID"] = 103,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103009] = {
		["ID"] = 103009,
		["Name"] = "奖励3",
		["ChapterID"] = 103,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[103010] = {
		["ID"] = 103010,
		["Name"] = "奖励1",
		["ChapterID"] = 103,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104001] = {
		["ID"] = 104001,
		["Name"] = "奖励2",
		["ChapterID"] = 104,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104002] = {
		["ID"] = 104002,
		["Name"] = "奖励3",
		["ChapterID"] = 104,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104003] = {
		["ID"] = 104003,
		["Name"] = "奖励1",
		["ChapterID"] = 104,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104004] = {
		["ID"] = 104004,
		["Name"] = "奖励2",
		["ChapterID"] = 104,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104005] = {
		["ID"] = 104005,
		["Name"] = "奖励3",
		["ChapterID"] = 104,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104006] = {
		["ID"] = 104006,
		["Name"] = "奖励1",
		["ChapterID"] = 104,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104007] = {
		["ID"] = 104007,
		["Name"] = "奖励2",
		["ChapterID"] = 104,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104008] = {
		["ID"] = 104008,
		["Name"] = "奖励3",
		["ChapterID"] = 104,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104009] = {
		["ID"] = 104009,
		["Name"] = "奖励1",
		["ChapterID"] = 104,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[104010] = {
		["ID"] = 104010,
		["Name"] = "奖励2",
		["ChapterID"] = 104,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[201001] = {
		["ID"] = 201001,
		["Name"] = "奖励3",
		["ChapterID"] = 201,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[201002] = {
		["ID"] = 201002,
		["Name"] = "奖励1",
		["ChapterID"] = 201,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[201003] = {
		["ID"] = 201003,
		["Name"] = "奖励2",
		["ChapterID"] = 201,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[201004] = {
		["ID"] = 201004,
		["Name"] = "奖励3",
		["ChapterID"] = 201,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[201005] = {
		["ID"] = 201005,
		["Name"] = "奖励1",
		["ChapterID"] = 201,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[202001] = {
		["ID"] = 202001,
		["Name"] = "奖励2",
		["ChapterID"] = 202,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[202002] = {
		["ID"] = 202002,
		["Name"] = "奖励3",
		["ChapterID"] = 202,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[202003] = {
		["ID"] = 202003,
		["Name"] = "奖励1",
		["ChapterID"] = 202,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[202004] = {
		["ID"] = 202004,
		["Name"] = "奖励2",
		["ChapterID"] = 202,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[202005] = {
		["ID"] = 202005,
		["Name"] = "奖励3",
		["ChapterID"] = 202,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[203001] = {
		["ID"] = 203001,
		["Name"] = "奖励1",
		["ChapterID"] = 203,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[203002] = {
		["ID"] = 203002,
		["Name"] = "奖励2",
		["ChapterID"] = 203,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[203003] = {
		["ID"] = 203003,
		["Name"] = "奖励3",
		["ChapterID"] = 203,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[203004] = {
		["ID"] = 203004,
		["Name"] = "奖励1",
		["ChapterID"] = 203,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[203005] = {
		["ID"] = 203005,
		["Name"] = "奖励2",
		["ChapterID"] = 203,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[204001] = {
		["ID"] = 204001,
		["Name"] = "奖励3",
		["ChapterID"] = 204,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[204002] = {
		["ID"] = 204002,
		["Name"] = "奖励1",
		["ChapterID"] = 204,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[204003] = {
		["ID"] = 204003,
		["Name"] = "奖励2",
		["ChapterID"] = 204,
		["NeedStar"] = 10,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[204004] = {
		["ID"] = 204004,
		["Name"] = "奖励3",
		["ChapterID"] = 204,
		["NeedStar"] = 15,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	},
	[204005] = {
		["ID"] = 204005,
		["Name"] = "奖励1",
		["ChapterID"] = 204,
		["NeedStar"] = 5,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}, {"Type": "CURRENCY", "Id": 4, "Amount": 100}]}]==]
	}
}