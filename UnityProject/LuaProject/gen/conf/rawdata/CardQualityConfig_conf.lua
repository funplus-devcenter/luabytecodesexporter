return {
	["N"] = {
		["Quality"] = "N",
		["IndexID"] = 1,
		["FaceCount"] = 3,
		["MaxLevel"] = 50
	},
	["R"] = {
		["Quality"] = "R",
		["IndexID"] = 2,
		["FaceCount"] = 3,
		["MaxLevel"] = 50
	},
	["SR"] = {
		["Quality"] = "SR",
		["IndexID"] = 3,
		["FaceCount"] = 6,
		["MaxLevel"] = 50
	},
	["SSR"] = {
		["Quality"] = "SSR",
		["IndexID"] = 4,
		["FaceCount"] = 6,
		["MaxLevel"] = 50
	}
}