return {
	[1] = {
		["ID"] = 1,
		["UseAccess"] = {102,101,105},
		["GetAccess"] = {403,201}
	},
	[2] = {
		["ID"] = 2,
		["UseAccess"] = {105},
		["GetAccess"] = {404,405}
	},
	[3] = {
		["ID"] = 3,
		["UseAccess"] = {408},
		["GetAccess"] = {407}
	},
	[4] = {
		["ID"] = 4,
		["UseAccess"] = {409},
		["GetAccess"] = {406}
	},
	[5] = {
		["ID"] = 5,
		["UseAccess"] = {410},
		["GetAccess"] = {407,303}
	},
	[6] = {
		["ID"] = 6,
		["UseAccess"] = {102},
		["GetAccess"] = {411}
	},
	[7] = {
		["ID"] = 7,
		["UseAccess"] = {104},
		["GetAccess"] = {105}
	},
	[8] = {
		["ID"] = 8,
		["UseAccess"] = {103},
		["GetAccess"] = {105}
	},
	[9] = {
		["ID"] = 9,
		["UseAccess"] = {101},
		["GetAccess"] = {447,412,413,414}
	},
	[10] = {
		["ID"] = 10,
		["UseAccess"] = {101},
		["GetAccess"] = {447,415,416,417}
	},
	[11] = {
		["ID"] = 11,
		["UseAccess"] = {101},
		["GetAccess"] = {447,418,419,420}
	},
	[12] = {
		["ID"] = 12,
		["UseAccess"] = {101},
		["GetAccess"] = {447,421,422,423}
	},
	[13] = {
		["ID"] = 13,
		["UseAccess"] = {101},
		["GetAccess"] = {447,424,425,426}
	},
	[14] = {
		["ID"] = 14,
		["UseAccess"] = {101},
		["GetAccess"] = {447,427428}
	},
	[15] = {
		["ID"] = 15,
		["UseAccess"] = {101},
		["GetAccess"] = {447,429,430}
	},
	[16] = {
		["ID"] = 16,
		["UseAccess"] = {101},
		["GetAccess"] = {447,431,432}
	},
	[17] = {
		["ID"] = 17,
		["UseAccess"] = {101},
		["GetAccess"] = {447,433,434}
	},
	[18] = {
		["ID"] = 18,
		["UseAccess"] = {101},
		["GetAccess"] = {447,435,436}
	},
	[19] = {
		["ID"] = 19,
		["UseAccess"] = {101},
		["GetAccess"] = {447,437,438}
	},
	[20] = {
		["ID"] = 20,
		["UseAccess"] = {101},
		["GetAccess"] = {447,439,440}
	},
	[21] = {
		["ID"] = 21,
		["UseAccess"] = {101},
		["GetAccess"] = {447,441,442}
	},
	[22] = {
		["ID"] = 22,
		["UseAccess"] = {101},
		["GetAccess"] = {447,443,444}
	},
	[23] = {
		["ID"] = 23,
		["UseAccess"] = {101},
		["GetAccess"] = {447,445,446}
	}
}