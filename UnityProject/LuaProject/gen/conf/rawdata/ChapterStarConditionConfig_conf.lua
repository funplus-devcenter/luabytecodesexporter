return {
	[1] = {
		["ID"] = 1,
		["Desc"] = "1.0",
		["Type"] = 1,
		["Params"] = [==[{"Amount": 1}]==]
	},
	[2] = {
		["ID"] = 2,
		["Desc"] = "1.0",
		["Type"] = 2,
		["Params"] = [==[{"Amount": 1}]==]
	},
	[3] = {
		["ID"] = 3,
		["Desc"] = "2.0",
		["Type"] = 3,
		["Params"] = [==[{"Amount": 1, "Color": 1}]==]
	},
	[4] = {
		["ID"] = 4,
		["Desc"] = "2.0",
		["Type"] = 4,
		["Params"] = [==[{"Amount": 1, "Color": 1}]==]
	},
	[5] = {
		["ID"] = 5,
		["Desc"] = "2.0",
		["Type"] = 5,
		["Params"] = [==[{"Amount": 1, "Color": 1}]==]
	}
}