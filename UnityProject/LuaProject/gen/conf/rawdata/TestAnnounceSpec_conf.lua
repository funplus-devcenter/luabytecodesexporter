return {
	[10000] = {
		["SpecId"] = 10000,
		["button_url"] = "www.163.com",
		["content1"] = "txt:abandon_forge_tip",
		["content2"] = "url:http://5b0988e595225.cdn.sohucs.com/images/20180928/cda37df083174f949c0fd574cd61506e.jpeg",
		["content3"] = "txt:display_reminder",
		["content4"] = "tex:Textures/Activity/ContinuousLogin01",
		["content5"] = "",
		["content6"] = "",
		["content7"] = "",
		["content8"] = "",
		["content9"] = "",
		["content10"] = "",
		["content11"] = "",
		["content12"] = "",
		["content13"] = "",
		["content14"] = "",
		["content15"] = ""
	},
	[10002] = {
		["SpecId"] = 10002,
		["button_url"] = "https://forms.gle/jAUBFMhUsaq1brJ46",
		["content1"] = "tex:Textures/Activity/board1",
		["content2"] = [==[txt:Testers Needed!

FarmTopia is looking for new Testers!
Requirements: 
- Good level of English;
- High-end iOS or Android device;
- Facebook account (for communication purposes);
- Willingness to play FarmTopia and give us feedback.

Testers can enjoy perks, such as: 
- Early Access to new game features;
- Direct contact with the Dev Team;
- Extra in-game items and rewards!

Click the button to apply now!
 ]==],
		["content3"] = "",
		["content4"] = "",
		["content5"] = "",
		["content6"] = "",
		["content7"] = "",
		["content8"] = "",
		["content9"] = "",
		["content10"] = "",
		["content11"] = "",
		["content12"] = "",
		["content13"] = "",
		["content14"] = "",
		["content15"] = ""
	},
	[20001] = {
		["SpecId"] = 20001,
		["button_url"] = "",
		["content1"] = [==[txt:
Dear Players,
 
Welcome to FarmTopia! I am Pie, the producer of the game. 

By the time you receive this e-mail, you’ll probably have finished the tutorial. I'd like to use this opportunity to give you a short introduction of FarmTopia and our Team.
]==],
		["content2"] = "url:http://farmtopia.akamaized.net/topia_images/post/DevLetter1_1.jpg",
		["content3"] = [==[txt:
FarmTopia is a mobile farm simulation game with roleplaying elements.

In it, you will take care of your own cozy Farm with a beautiful private beach located on the mysterious Nemo Island. 

You will also get to know the island’s citizens and explore their stories as you experience various adventures together. 
]==],
		["content4"] = "url:http://farmtopia.akamaized.net/topia_images/post/DevLetter1_2.jpg",
		["content5"] = [==[txt:
Once you repair the Train Station south of your Farm, you’ll be able to enter the multiplayer mode. 

Players from all around the world will visit your Farm and help you out with your chores. You can also visit them back and offer your assistance to them.

Joining forces with other Players is what makes the FarmTopia experience exciting and unique!
]==],
		["content6"] = "url:http://farmtopia.akamaized.net/topia_images/post/DevLetter1_3.jpg",
		["content7"] = [==[txt:
FarmTopia is developed by a small team consisting of ~20 members from all over the world. 

Before joining the FarmTopia team, many of us worked on another farming game, which has accumulated ~100 million players over the 8 years since its release. 

This experience has taught us that good games are created not just by their developers, but also by their community. 

We’ve learned to cherish feedback from our players, and are looking forward to hear your opinions!
]==],
		["content8"] = "url:http://farmtopia.akamaized.net/topia_images/post/DevLetter1_4.jpg",
		["content9"] = [==[txt:
If you encounter any problems during the game, you can contact our Customer Service by tapping the Help button in the Settings Panel.

If you have any suggestions on how we can improve FarmTopia, please feel free to contact me directly: 
fei.hai@centurygame.com
 
I wish you a pleasant farming experience!
 
Yours Sincerely,
Pie from the FarmTopia Dev Team
 
PS.: Don’t miss tomorrow’s Dev Diary with insights from our <color=#ff5741>Story Designer.</color> Make sure to check your mailbox regularly for more updates from our Team!
]==],
		["content10"] = "",
		["content11"] = "",
		["content12"] = "",
		["content13"] = "",
		["content14"] = "",
		["content15"] = ""
	},
	[20002] = {
		["SpecId"] = 20002,
		["button_url"] = "",
		["content1"] = [==[txt:
Dear Players,
 
Welcome to FarmTopia! I am Pie, the producer of the game. 

By the time you receive this e-mail, you’ll probably have finished the tutorial. I'd like to use this opportunity to give you a short introduction of FarmTopia and our Team.
]==],
		["content2"] = "url:http://farmtopia.akamaized.net/topia_images/post/DevLetter1_1.jpg",
		["content3"] = [==[txt:
FarmTopia is a mobile farm simulation game with roleplaying elements.

In it, you will take care of your own cozy Farm with a beautiful private beach located on the mysterious Nemo Island. 

You will also get to know the island’s citizens and explore their stories as you experience various adventures together. 
]==],
		["content4"] = "url:http://farmtopia.akamaized.net/topia_images/post/DevLetter1_2.jpg",
		["content5"] = [==[txt:
Once you repair the Train Station south of your Farm, you’ll be able to enter the multiplayer mode. 

Players from all around the world will visit your Farm and help you out with your chores. You can also visit them back and offer your assistance to them.

Joining forces with other Players is what makes the FarmTopia experience exciting and unique!
]==],
		["content6"] = "url:http://farmtopia.akamaized.net/topia_images/post/DevLetter1_3.jpg",
		["content7"] = [==[txt:
FarmTopia is developed by a small team consisting of ~20 members from all over the world. 

Before joining the FarmTopia team, many of us worked on another farming game, which has accumulated ~100 million players over the 8 years since its release. 

This experience has taught us that good games are created not just by their developers, but also by their community. 

We’ve learned to cherish feedback from our players, and are looking forward to hear your opinions!
]==],
		["content8"] = "url:http://farmtopia.akamaized.net/topia_images/post/DevLetter1_4.jpg",
		["content9"] = [==[dasfadfasdfsadfdsafsdfsfsdfdsafjalsdflajsdflasjdflkasjdfkljalskdfjlsa;jflk;sadjflksajdfklsaf
dfasdjflajdflkajfkla;sjdkfl;as
dsfasdfsdafasdf
dasfdsafsadfsafdsafsdf]==],
		["content10"] = "",
		["content11"] = "",
		["content12"] = "",
		["content13"] = "",
		["content14"] = "",
		["content15"] = ""
	}
}