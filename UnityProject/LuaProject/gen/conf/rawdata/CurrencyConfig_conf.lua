return {
	[1] = {
		["ID"] = 1,
		["Name"] = "EXP",
		["Source"] = {"100101"},
		["ResourceGuide"] = 1,
		["CanCost"] = true,
		["CanShow"] = false,
		["Desc"] = "玩家经验值",
		["Icon"] = "item_exp"
	},
	[2] = {
		["ID"] = 2,
		["Name"] = "金币",
		["Source"] = {"100201","100202"},
		["ResourceGuide"] = 1,
		["CanCost"] = true,
		["CanShow"] = true,
		["Desc"] = "金币",
		["Icon"] = "item_coin"
	},
	[3] = {
		["ID"] = 3,
		["Name"] = "钻石",
		["Source"] = {"100301.0"},
		["ResourceGuide"] = 2,
		["CanCost"] = true,
		["CanShow"] = true,
		["Desc"] = "钻石",
		["Icon"] = "item_diamond"
	},
	[4] = {
		["ID"] = 4,
		["Name"] = "体力",
		["Source"] = {"200101.0"},
		["ResourceGuide"] = 1,
		["CanCost"] = true,
		["CanShow"] = false,
		["Desc"] = "体力",
		["Icon"] = "item_power"
	},
	[5] = {
		["ID"] = 5,
		["Name"] = "回忆手札",
		["Source"] = {"200101.0"},
		["ResourceGuide"] = 3,
		["CanCost"] = true,
		["CanShow"] = true,
		["Desc"] = "观看支线故事！",
		["Icon"] = "item_idea"
	},
	[6] = {
		["ID"] = 6,
		["Name"] = "玫瑰手札",
		["Source"] = {"Null"},
		["ResourceGuide"] = 4,
		["CanCost"] = true,
		["CanShow"] = true,
		["Desc"] = "观看主线故事！",
		["Icon"] = "item_storycoin"
	},
	[7] = {
		["ID"] = 7,
		["Name"] = "活跃度",
		["Source"] = {"Null"},
		["ResourceGuide"] = 1,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "活跃度",
		["Icon"] = "item_exp"
	},
	[8] = {
		["ID"] = 8,
		["Name"] = "邀请函",
		["Source"] = {"Null"},
		["ResourceGuide"] = 5,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "可解锁书卡封面",
		["Icon"] = "item_invite"
	},
	[101] = {
		["ID"] = 101,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励1",
		["Icon"] = "item_exp"
	},
	[102] = {
		["ID"] = 102,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励2",
		["Icon"] = "item_exp"
	},
	[103] = {
		["ID"] = 103,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励3",
		["Icon"] = "item_exp"
	},
	[104] = {
		["ID"] = 104,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励4",
		["Icon"] = "item_exp"
	},
	[105] = {
		["ID"] = 105,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励5",
		["Icon"] = "item_exp"
	},
	[106] = {
		["ID"] = 106,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励6",
		["Icon"] = "item_exp"
	},
	[107] = {
		["ID"] = 107,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励7",
		["Icon"] = "item_exp"
	},
	[108] = {
		["ID"] = 108,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励8",
		["Icon"] = "item_exp"
	},
	[109] = {
		["ID"] = 109,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励9",
		["Icon"] = "item_exp"
	},
	[110] = {
		["ID"] = 110,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励10",
		["Icon"] = "item_exp"
	},
	[111] = {
		["ID"] = 111,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励11",
		["Icon"] = "item_exp"
	},
	[112] = {
		["ID"] = 112,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励12",
		["Icon"] = "item_exp"
	},
	[113] = {
		["ID"] = 113,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励13",
		["Icon"] = "item_exp"
	},
	[114] = {
		["ID"] = 114,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励14",
		["Icon"] = "item_exp"
	},
	[115] = {
		["ID"] = 115,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励15",
		["Icon"] = "item_exp"
	},
	[116] = {
		["ID"] = 116,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励16",
		["Icon"] = "item_exp"
	},
	[117] = {
		["ID"] = 117,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励17",
		["Icon"] = "item_exp"
	},
	[118] = {
		["ID"] = 118,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励18",
		["Icon"] = "item_exp"
	},
	[119] = {
		["ID"] = 119,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励19",
		["Icon"] = "item_exp"
	},
	[120] = {
		["ID"] = 120,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励20",
		["Icon"] = "item_exp"
	},
	[121] = {
		["ID"] = 121,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励20",
		["Icon"] = "item_exp"
	},
	[122] = {
		["ID"] = 122,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励21",
		["Icon"] = "item_exp"
	},
	[123] = {
		["ID"] = 123,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励22",
		["Icon"] = "item_exp"
	},
	[124] = {
		["ID"] = 124,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励23",
		["Icon"] = "item_exp"
	},
	[125] = {
		["ID"] = 125,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励24",
		["Icon"] = "item_exp"
	},
	[126] = {
		["ID"] = 126,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励25",
		["Icon"] = "item_exp"
	},
	[127] = {
		["ID"] = 127,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励26",
		["Icon"] = "item_exp"
	},
	[128] = {
		["ID"] = 128,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励27",
		["Icon"] = "item_exp"
	},
	[129] = {
		["ID"] = 129,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励28",
		["Icon"] = "item_exp"
	},
	[130] = {
		["ID"] = 130,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励29",
		["Icon"] = "item_exp"
	},
	[131] = {
		["ID"] = 131,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励30",
		["Icon"] = "item_exp"
	},
	[132] = {
		["ID"] = 132,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励31",
		["Icon"] = "item_exp"
	},
	[133] = {
		["ID"] = 133,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励32",
		["Icon"] = "item_exp"
	},
	[134] = {
		["ID"] = 134,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励33",
		["Icon"] = "item_exp"
	},
	[135] = {
		["ID"] = 135,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励34",
		["Icon"] = "item_exp"
	},
	[136] = {
		["ID"] = 136,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励35",
		["Icon"] = "item_exp"
	},
	[137] = {
		["ID"] = 137,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励36",
		["Icon"] = "item_exp"
	},
	[138] = {
		["ID"] = 138,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励37",
		["Icon"] = "item_exp"
	},
	[139] = {
		["ID"] = 139,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励38",
		["Icon"] = "item_exp"
	},
	[140] = {
		["ID"] = 140,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励39",
		["Icon"] = "item_exp"
	},
	[141] = {
		["ID"] = 141,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励40",
		["Icon"] = "item_exp"
	},
	[142] = {
		["ID"] = 142,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励41",
		["Icon"] = "item_exp"
	},
	[143] = {
		["ID"] = 143,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励42",
		["Icon"] = "item_exp"
	},
	[144] = {
		["ID"] = 144,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励43",
		["Icon"] = "item_exp"
	},
	[145] = {
		["ID"] = 145,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励44",
		["Icon"] = "item_exp"
	},
	[146] = {
		["ID"] = 146,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励45",
		["Icon"] = "item_exp"
	},
	[147] = {
		["ID"] = 147,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励46",
		["Icon"] = "item_exp"
	},
	[148] = {
		["ID"] = 148,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励47",
		["Icon"] = "item_exp"
	},
	[149] = {
		["ID"] = 149,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励48",
		["Icon"] = "item_exp"
	},
	[150] = {
		["ID"] = 150,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励49",
		["Icon"] = "item_exp"
	},
	[151] = {
		["ID"] = 151,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励50",
		["Icon"] = "item_exp"
	},
	[152] = {
		["ID"] = 152,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励51",
		["Icon"] = "item_exp"
	},
	[153] = {
		["ID"] = 153,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励52",
		["Icon"] = "item_exp"
	},
	[154] = {
		["ID"] = 154,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励53",
		["Icon"] = "item_exp"
	},
	[155] = {
		["ID"] = 155,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励54",
		["Icon"] = "item_exp"
	},
	[156] = {
		["ID"] = 156,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励55",
		["Icon"] = "item_exp"
	},
	[157] = {
		["ID"] = 157,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励56",
		["Icon"] = "item_exp"
	},
	[158] = {
		["ID"] = 158,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励57",
		["Icon"] = "item_exp"
	},
	[159] = {
		["ID"] = 159,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励58",
		["Icon"] = "item_exp"
	},
	[160] = {
		["ID"] = 160,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励59",
		["Icon"] = "item_exp"
	},
	[161] = {
		["ID"] = 161,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励60",
		["Icon"] = "item_exp"
	},
	[162] = {
		["ID"] = 162,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励61",
		["Icon"] = "item_exp"
	},
	[163] = {
		["ID"] = 163,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励62",
		["Icon"] = "item_exp"
	},
	[164] = {
		["ID"] = 164,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励63",
		["Icon"] = "item_exp"
	},
	[165] = {
		["ID"] = 165,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励64",
		["Icon"] = "item_exp"
	},
	[166] = {
		["ID"] = 166,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励65",
		["Icon"] = "item_exp"
	},
	[167] = {
		["ID"] = 167,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励66",
		["Icon"] = "item_exp"
	},
	[168] = {
		["ID"] = 168,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励67",
		["Icon"] = "item_exp"
	},
	[169] = {
		["ID"] = 169,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励68",
		["Icon"] = "item_exp"
	},
	[170] = {
		["ID"] = 170,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励69",
		["Icon"] = "item_exp"
	},
	[171] = {
		["ID"] = 171,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励70",
		["Icon"] = "item_exp"
	},
	[172] = {
		["ID"] = 172,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励71",
		["Icon"] = "item_exp"
	},
	[173] = {
		["ID"] = 173,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励72",
		["Icon"] = "item_exp"
	},
	[174] = {
		["ID"] = 174,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励73",
		["Icon"] = "item_exp"
	},
	[175] = {
		["ID"] = 175,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励74",
		["Icon"] = "item_exp"
	},
	[176] = {
		["ID"] = 176,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励75",
		["Icon"] = "item_exp"
	},
	[177] = {
		["ID"] = 177,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励76",
		["Icon"] = "item_exp"
	},
	[178] = {
		["ID"] = 178,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励77",
		["Icon"] = "item_exp"
	},
	[179] = {
		["ID"] = 179,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励78",
		["Icon"] = "item_exp"
	},
	[180] = {
		["ID"] = 180,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励79",
		["Icon"] = "item_exp"
	},
	[181] = {
		["ID"] = 181,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励80",
		["Icon"] = "item_exp"
	},
	[182] = {
		["ID"] = 182,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励81",
		["Icon"] = "item_exp"
	},
	[183] = {
		["ID"] = 183,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励82",
		["Icon"] = "item_exp"
	},
	[184] = {
		["ID"] = 184,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励83",
		["Icon"] = "item_exp"
	},
	[185] = {
		["ID"] = 185,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励84",
		["Icon"] = "item_exp"
	},
	[186] = {
		["ID"] = 186,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励85",
		["Icon"] = "item_exp"
	},
	[187] = {
		["ID"] = 187,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励86",
		["Icon"] = "item_exp"
	},
	[188] = {
		["ID"] = 188,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励87",
		["Icon"] = "item_exp"
	},
	[189] = {
		["ID"] = 189,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励88",
		["Icon"] = "item_exp"
	},
	[190] = {
		["ID"] = 190,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励89",
		["Icon"] = "item_exp"
	},
	[191] = {
		["ID"] = 191,
		["Name"] = "语音奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励89",
		["Icon"] = "item_exp"
	},
	[201] = {
		["ID"] = 201,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励90",
		["Icon"] = "item_exp"
	},
	[202] = {
		["ID"] = 202,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励91",
		["Icon"] = "item_exp"
	},
	[203] = {
		["ID"] = 203,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励92",
		["Icon"] = "item_exp"
	},
	[204] = {
		["ID"] = 204,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励93",
		["Icon"] = "item_exp"
	},
	[205] = {
		["ID"] = 205,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励94",
		["Icon"] = "item_exp"
	},
	[206] = {
		["ID"] = 206,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励95",
		["Icon"] = "item_exp"
	},
	[207] = {
		["ID"] = 207,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励96",
		["Icon"] = "item_exp"
	},
	[208] = {
		["ID"] = 208,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励97",
		["Icon"] = "item_exp"
	},
	[209] = {
		["ID"] = 209,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励98",
		["Icon"] = "item_exp"
	},
	[210] = {
		["ID"] = 210,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励99",
		["Icon"] = "item_exp"
	},
	[211] = {
		["ID"] = 211,
		["Name"] = "故事奖励",
		["Source"] = {"Null"},
		["ResourceGuide"] = 0,
		["CanCost"] = false,
		["CanShow"] = false,
		["Desc"] = "故事奖励100",
		["Icon"] = "item_exp"
	}
}