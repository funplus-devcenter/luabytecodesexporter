return {
	[101] = {
		["ID"] = 101,
		["Name"] = "事件一 《叶洛莉兰》",
		["Desc"] = "《叶洛莉兰》",
		["Res"] = "Icon_1",
		["Type"] = 1,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["NextChapterID"] = 102,
		["ParentChapterID"] = 0
	},
	[102] = {
		["ID"] = 102,
		["Name"] = "事件二 《无木之森》",
		["Desc"] = "《无木之森》",
		["Res"] = "Icon_2",
		["Type"] = 1,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["NextChapterID"] = 103,
		["ParentChapterID"] = 101
	},
	[103] = {
		["ID"] = 103,
		["Name"] = "事件三 《喵计划》",
		["Desc"] = "《喵计划》",
		["Res"] = "Icon_3",
		["Type"] = 1,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["NextChapterID"] = 104,
		["ParentChapterID"] = 102
	},
	[104] = {
		["ID"] = 104,
		["Name"] = "事件四 《法老的纹章》",
		["Desc"] = "《法老的纹章》",
		["Res"] = "Icon_4",
		["Type"] = 1,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["NextChapterID"] = 0,
		["ParentChapterID"] = 103
	},
	[201] = {
		["ID"] = 201,
		["Name"] = "事件一：困难",
		["Desc"] = "《叶洛莉兰》",
		["Res"] = "Icon_1",
		["Type"] = 2,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["NextChapterID"] = 202,
		["ParentChapterID"] = 101
	},
	[202] = {
		["ID"] = 202,
		["Name"] = "事件二：困难",
		["Desc"] = "《无木之森》",
		["Res"] = "Icon_2",
		["Type"] = 2,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["NextChapterID"] = 203,
		["ParentChapterID"] = 102
	},
	[203] = {
		["ID"] = 203,
		["Name"] = "事件三：困难",
		["Desc"] = "《喵计划》",
		["Res"] = "Icon_3",
		["Type"] = 2,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["NextChapterID"] = 204,
		["ParentChapterID"] = 103
	},
	[204] = {
		["ID"] = 204,
		["Name"] = "事件四：困难",
		["Desc"] = "《法老的纹章》",
		["Res"] = "Icon_4",
		["Type"] = 2,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["NextChapterID"] = 0,
		["ParentChapterID"] = 104
	},
	[301] = {
		["ID"] = 301,
		["Name"] = "我要金币",
		["Desc"] = "我要金币",
		["Res"] = "Icon_5",
		["Type"] = 3,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 10,
		["NextChapterID"] = 0,
		["ParentChapterID"] = 0
	},
	[302] = {
		["ID"] = 302,
		["Name"] = "我要经验",
		["Desc"] = "我要经验",
		["Res"] = "Icon_6",
		["Type"] = 3,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 10,
		["NextChapterID"] = 0,
		["ParentChapterID"] = 0
	}
}