return {
	[1] = {
		["ID"] = 1,
		["LotteryId"] = 1,
		["Card"] = 100001,
		["Double"] = 1
	},
	[2] = {
		["ID"] = 2,
		["LotteryId"] = 1,
		["Card"] = 100002,
		["Double"] = 0
	},
	[3] = {
		["ID"] = 3,
		["LotteryId"] = 1,
		["Card"] = 100003,
		["Double"] = 0
	},
	[4] = {
		["ID"] = 4,
		["LotteryId"] = 1,
		["Card"] = 100004,
		["Double"] = 0
	},
	[5] = {
		["ID"] = 5,
		["LotteryId"] = 1,
		["Card"] = 100005,
		["Double"] = 0
	},
	[6] = {
		["ID"] = 6,
		["LotteryId"] = 1,
		["Card"] = 200001,
		["Double"] = 1
	},
	[7] = {
		["ID"] = 7,
		["LotteryId"] = 1,
		["Card"] = 200002,
		["Double"] = 0
	},
	[8] = {
		["ID"] = 8,
		["LotteryId"] = 1,
		["Card"] = 200003,
		["Double"] = 0
	},
	[9] = {
		["ID"] = 9,
		["LotteryId"] = 1,
		["Card"] = 200004,
		["Double"] = 0
	},
	[10] = {
		["ID"] = 10,
		["LotteryId"] = 1,
		["Card"] = 200005,
		["Double"] = 0
	},
	[11] = {
		["ID"] = 11,
		["LotteryId"] = 2,
		["Card"] = 100001,
		["Double"] = 1
	},
	[12] = {
		["ID"] = 12,
		["LotteryId"] = 2,
		["Card"] = 100002,
		["Double"] = 0
	},
	[13] = {
		["ID"] = 13,
		["LotteryId"] = 2,
		["Card"] = 100003,
		["Double"] = 0
	},
	[14] = {
		["ID"] = 14,
		["LotteryId"] = 2,
		["Card"] = 100004,
		["Double"] = 0
	},
	[15] = {
		["ID"] = 15,
		["LotteryId"] = 2,
		["Card"] = 100005,
		["Double"] = 0
	},
	[16] = {
		["ID"] = 16,
		["LotteryId"] = 3,
		["Card"] = 100001,
		["Double"] = 0
	},
	[17] = {
		["ID"] = 17,
		["LotteryId"] = 3,
		["Card"] = 100002,
		["Double"] = 0
	},
	[18] = {
		["ID"] = 18,
		["LotteryId"] = 3,
		["Card"] = 100003,
		["Double"] = 0
	},
	[19] = {
		["ID"] = 19,
		["LotteryId"] = 3,
		["Card"] = 100004,
		["Double"] = 0
	},
	[20] = {
		["ID"] = 20,
		["LotteryId"] = 3,
		["Card"] = 100005,
		["Double"] = 0
	}
}