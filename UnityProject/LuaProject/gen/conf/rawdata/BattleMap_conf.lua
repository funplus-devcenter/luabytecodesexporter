return {
	[100001] = {
		["ID"] = 100001,
		["Name"] = "关卡1",
		["AutoBoardID"] = 100001,
		["ManualBoardID"] = 0,
		["MonsterID"] = 	{
					{
				["value"] = {100002,100002,100002}
			},
					{
				["value"] = {100002,100002,100002}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 2}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==],
		["HpPercentage"] = 1500,
		["AtkPercentage"] = 1500,
		["DefPercentage"] = 1500
	},
	[100002] = {
		["ID"] = 100002,
		["Name"] = "关卡2",
		["AutoBoardID"] = 0,
		["ManualBoardID"] = 100001,
		["MonsterID"] = 	{
					{
				["value"] = {100002,100002,100002}
			},
					{
				["value"] = {100002,100002,100002}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 2}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==],
		["HpPercentage"] = 2000,
		["AtkPercentage"] = 2000,
		["DefPercentage"] = 2000
	}
}