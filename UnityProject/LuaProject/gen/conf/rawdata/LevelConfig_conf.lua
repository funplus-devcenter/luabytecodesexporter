return {
	[1] = {
		["Level"] = 1,
		["Exp"] = 250,
		["Energy"] = 82,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[2] = {
		["Level"] = 2,
		["Exp"] = 650,
		["Energy"] = 84,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[3] = {
		["Level"] = 3,
		["Exp"] = 1270,
		["Energy"] = 86,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[4] = {
		["Level"] = 4,
		["Exp"] = 1930,
		["Energy"] = 88,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[5] = {
		["Level"] = 5,
		["Exp"] = 2630,
		["Energy"] = 90,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[6] = {
		["Level"] = 6,
		["Exp"] = 3370,
		["Energy"] = 91,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[7] = {
		["Level"] = 7,
		["Exp"] = 4150,
		["Energy"] = 92,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[8] = {
		["Level"] = 8,
		["Exp"] = 4970,
		["Energy"] = 93,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[9] = {
		["Level"] = 9,
		["Exp"] = 5830,
		["Energy"] = 94,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[10] = {
		["Level"] = 10,
		["Exp"] = 6730,
		["Energy"] = 95,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[11] = {
		["Level"] = 11,
		["Exp"] = 7670,
		["Energy"] = 96,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[12] = {
		["Level"] = 12,
		["Exp"] = 8650,
		["Energy"] = 97,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[13] = {
		["Level"] = 13,
		["Exp"] = 9670,
		["Energy"] = 98,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[14] = {
		["Level"] = 14,
		["Exp"] = 10730,
		["Energy"] = 99,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[15] = {
		["Level"] = 15,
		["Exp"] = 11830,
		["Energy"] = 100,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[16] = {
		["Level"] = 16,
		["Exp"] = 12970,
		["Energy"] = 101,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[17] = {
		["Level"] = 17,
		["Exp"] = 14150,
		["Energy"] = 102,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[18] = {
		["Level"] = 18,
		["Exp"] = 15370,
		["Energy"] = 103,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[19] = {
		["Level"] = 19,
		["Exp"] = 16630,
		["Energy"] = 104,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[20] = {
		["Level"] = 20,
		["Exp"] = 17930,
		["Energy"] = 105,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[21] = {
		["Level"] = 21,
		["Exp"] = 19270,
		["Energy"] = 106,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[22] = {
		["Level"] = 22,
		["Exp"] = 20650,
		["Energy"] = 107,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[23] = {
		["Level"] = 23,
		["Exp"] = 22070,
		["Energy"] = 108,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[24] = {
		["Level"] = 24,
		["Exp"] = 23530,
		["Energy"] = 109,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[25] = {
		["Level"] = 25,
		["Exp"] = 25030,
		["Energy"] = 110,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[26] = {
		["Level"] = 26,
		["Exp"] = 26570,
		["Energy"] = 111,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[27] = {
		["Level"] = 27,
		["Exp"] = 28150,
		["Energy"] = 112,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[28] = {
		["Level"] = 28,
		["Exp"] = 29770,
		["Energy"] = 113,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[29] = {
		["Level"] = 29,
		["Exp"] = 31445,
		["Energy"] = 114,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[30] = {
		["Level"] = 30,
		["Exp"] = 33175,
		["Energy"] = 115,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[31] = {
		["Level"] = 31,
		["Exp"] = 34960,
		["Energy"] = 116,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[32] = {
		["Level"] = 32,
		["Exp"] = 36800,
		["Energy"] = 117,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[33] = {
		["Level"] = 33,
		["Exp"] = 38695,
		["Energy"] = 118,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[34] = {
		["Level"] = 34,
		["Exp"] = 40645,
		["Energy"] = 119,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[35] = {
		["Level"] = 35,
		["Exp"] = 42745,
		["Energy"] = 120,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[36] = {
		["Level"] = 36,
		["Exp"] = 44995,
		["Energy"] = 120,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[37] = {
		["Level"] = 37,
		["Exp"] = 47395,
		["Energy"] = 120,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[38] = {
		["Level"] = 38,
		["Exp"] = 49945,
		["Energy"] = 120,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[39] = {
		["Level"] = 39,
		["Exp"] = 52645,
		["Energy"] = 120,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[40] = {
		["Level"] = 40,
		["Exp"] = 55495,
		["Energy"] = 121,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[41] = {
		["Level"] = 41,
		["Exp"] = 58495,
		["Energy"] = 121,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[42] = {
		["Level"] = 42,
		["Exp"] = 61645,
		["Energy"] = 121,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[43] = {
		["Level"] = 43,
		["Exp"] = 64945,
		["Energy"] = 121,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[44] = {
		["Level"] = 44,
		["Exp"] = 68395,
		["Energy"] = 121,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[45] = {
		["Level"] = 45,
		["Exp"] = 71995,
		["Energy"] = 122,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[46] = {
		["Level"] = 46,
		["Exp"] = 75745,
		["Energy"] = 122,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[47] = {
		["Level"] = 47,
		["Exp"] = 79645,
		["Energy"] = 122,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[48] = {
		["Level"] = 48,
		["Exp"] = 83695,
		["Energy"] = 122,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[49] = {
		["Level"] = 49,
		["Exp"] = 87895,
		["Energy"] = 122,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	},
	[50] = {
		["Level"] = 50,
		["Exp"] = 92245,
		["Energy"] = 123,
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 80}]}]==]
	}
}