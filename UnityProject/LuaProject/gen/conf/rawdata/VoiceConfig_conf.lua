return {
	[101] = {
		["ID"] = 101,
		["ResID"] = "voice_1_2",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 101, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "七月初七，秋意渐浓，记得添衣。"
	},
	[102] = {
		["ID"] = 102,
		["ResID"] = "voice_1_3",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 102, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "我会一直站在光影下，你一眼就能看到的地方，等你回家。"
	},
	[103] = {
		["ID"] = 103,
		["ResID"] = "voice_1_4",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 103, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "那夜的乌鸦叫得格外凄惨，余音切切，你听见了吗？我听得格外清楚，每一声都扎在心上。"
	},
	[104] = {
		["ID"] = 104,
		["ResID"] = "voice_1_11",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 104, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "洋桔梗与玫瑰的区别，就如同理智与情感。"
	},
	[105] = {
		["ID"] = 105,
		["ResID"] = "voice_1_12",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 105, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "莎士比亚用羽毛笔写下十四行诗，一百五十四首，连他无法把你比喻成夏天。"
	},
	[106] = {
		["ID"] = 106,
		["ResID"] = "voice_1_13",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 106, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "我把字条钉上罗斯曼桥头，没有落款，却期待有人能看见。"
	},
	[107] = {
		["ID"] = 107,
		["ResID"] = "voice_1_16",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 107, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "这样的月色值得被放在心里珍藏，就收在你的旁边。"
	},
	[108] = {
		["ID"] = 108,
		["ResID"] = "voice_1_17",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 108, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "令我不安的源头，是你，不过我甘之如饴。"
	},
	[109] = {
		["ID"] = 109,
		["ResID"] = "voice_1_18",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 109, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "如果……月亮升起的时候我还没有回来，就去梦里找我。"
	},
	[110] = {
		["ID"] = 110,
		["ResID"] = "voice_1_21",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 110, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "这里原本是我的秘密基地，以后……就是我们的了。"
	},
	[111] = {
		["ID"] = 111,
		["ResID"] = "voice_1_22",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 111, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "夏日的气息和你，都是写歌的灵感来源。"
	},
	[112] = {
		["ID"] = 112,
		["ResID"] = "voice_1_23",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 112, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "天台的涂鸦墙上，不只藏着你一个人的秘密。"
	},
	[113] = {
		["ID"] = 113,
		["ResID"] = "voice_1_26",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 113, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "这扇门打开之前，我就知道是你来了……嗯，你的脚步声跟他们都不一样。"
	},
	[114] = {
		["ID"] = 114,
		["ResID"] = "voice_1_27",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 114, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "那只兔子死了之后，我再也没有养过任何一只小动物。"
	},
	[115] = {
		["ID"] = 115,
		["ResID"] = "voice_1_28",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 115, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "心脏并不是我唯一的弱点，你才是。"
	},
	[116] = {
		["ID"] = 116,
		["ResID"] = "voice_1_31",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 116, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "至少在我面前，你可以不用那么逞强。"
	},
	[117] = {
		["ID"] = 117,
		["ResID"] = "voice_1_32",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 117, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "不要试图对我撒谎，无论是作为医生还是作为顾淮，我都非常了解你。"
	},
	[118] = {
		["ID"] = 118,
		["ResID"] = "voice_1_33",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 118, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "在我的办公室里睡觉，可不是常规治疗项目，要额外收钱的。"
	},
	[119] = {
		["ID"] = 119,
		["ResID"] = "voice_1_106",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 119, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "草莓味的人造血浆你要尝一下吗？"
	},
	[120] = {
		["ID"] = 120,
		["ResID"] = "voice_1_37",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 120, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "你的每一个表情，都值得被好好收藏。"
	},
	[121] = {
		["ID"] = 121,
		["ResID"] = "voice_1_108",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 121, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "不要让路边的野猫进我的办公室，掉毛。"
	},
	[122] = {
		["ID"] = 122,
		["ResID"] = "voice_1_41",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 122, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "雏菊的花语，是纯洁、离别和深藏心底的暗恋。"
	},
	[123] = {
		["ID"] = 123,
		["ResID"] = "voice_1_42",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 123, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "独自运转的齿轮，被遗忘在世界和时间之外。"
	},
	[124] = {
		["ID"] = 124,
		["ResID"] = "voice_1_43",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 124, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "这个只有我存在的无趣世界，因为你的到来，才第一次变得鲜活起来。"
	},
	[125] = {
		["ID"] = 125,
		["ResID"] = "voice_1_46",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 125, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "你被写进我的基因序列里，无法修改。"
	},
	[126] = {
		["ID"] = 126,
		["ResID"] = "voice_1_47",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 126, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "觉醒的序章写满了疼痛，宇宙塌缩预示新世纪的纪元。"
	},
	[127] = {
		["ID"] = 127,
		["ResID"] = "voice_1_48",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 127, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "当永恒不再是一个虚无空白的词，你会害怕吗？"
	},
	[128] = {
		["ID"] = 128,
		["ResID"] = "voice_1_51",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 128, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "心理学认为，遗忘是由于情绪压抑造成的，如果释放压抑，记忆就能够恢复。"
	},
	[129] = {
		["ID"] = 129,
		["ResID"] = "voice_1_52",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 129, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "受伤的蝴蝶无法回到风里，只能安静停歇在我的掌心。"
	},
	[130] = {
		["ID"] = 130,
		["ResID"] = "voice_1_53",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 130, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "只有在患者完全信任医生的情况下，才能进入那个提前编织的虚幻梦境。"
	},
	[131] = {
		["ID"] = 131,
		["ResID"] = "voice_1_56",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 131, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "这个色号为什么从没有见你涂过？"
	},
	[132] = {
		["ID"] = 132,
		["ResID"] = "voice_1_57",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 132, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "晚安两个字，要不想睡的人说给困的人听。"
	},
	[133] = {
		["ID"] = 133,
		["ResID"] = "voice_1_60",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 133, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "没有涂色的画让人遐想，没有结局的故事令人着迷。"
	},
	[134] = {
		["ID"] = 134,
		["ResID"] = "voice_1_61",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 134, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "上帝将贝壳藏到人间，于是便有了珍珠跟你。"
	},
	[135] = {
		["ID"] = 135,
		["ResID"] = "voice_1_64",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 135, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "其实我们的大脑本身就有很多人格，只是你看到的我，占了上风而已。"
	},
	[136] = {
		["ID"] = 136,
		["ResID"] = "voice_1_65",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 136, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "一个身体里住着好几个灵魂的才被叫做“多重人格”，而我的灵魂早已献祭。"
	},
	[137] = {
		["ID"] = 137,
		["ResID"] = "voice_1_68",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 137, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "读懂数学题很简单，想要读懂你却很难。"
	},
	[138] = {
		["ID"] = 138,
		["ResID"] = "voice_1_69",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 138, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "昨天的习题册帮你改好了。不过……第25页写满我的名字是什么意思？"
	},
	[139] = {
		["ID"] = 139,
		["ResID"] = "voice_1_72",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 139, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "怎么一直盯着我看，我脸上有什么东西吗？"
	},
	[140] = {
		["ID"] = 140,
		["ResID"] = "voice_1_73",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 140, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "经典的旋律，往往是用最简单的音符谱成的。"
	},
	[141] = {
		["ID"] = 141,
		["ResID"] = "voice_1_7",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 141, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "不必喊我少爷，叫我名字即可。"
	},
	[142] = {
		["ID"] = 142,
		["ResID"] = "voice_1_8",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 142, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "你点亮了我黯淡人生里最后一束光，我会守护不让它熄灭。"
	},
	[143] = {
		["ID"] = 143,
		["ResID"] = "voice_1_75",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 143, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "初次见面，我叫顾淮。"
	},
	[144] = {
		["ID"] = 144,
		["ResID"] = "voice_1_78",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 144, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "这是你的书吗？"
	},
	[145] = {
		["ID"] = 145,
		["ResID"] = "voice_1_81",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 145, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "我是顾医生。"
	},
	[146] = {
		["ID"] = 146,
		["ResID"] = "voice_1_84",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 146, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "我想把你看清楚。"
	},
	[147] = {
		["ID"] = 147,
		["ResID"] = "voice_1_87",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 147, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 1,
		["Desc"] = "我快下班了，等会去哪？"
	},
	[148] = {
		["ID"] = 148,
		["ResID"] = "voice_2_2",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 148, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "退左、进右，试探跟暧昧交错在每一步华尔兹里。"
	},
	[149] = {
		["ID"] = 149,
		["ResID"] = "voice_2_3",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 149, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "西洋童话里午夜就会消失的辛蒂瑞拉，是你吗？"
	},
	[150] = {
		["ID"] = 150,
		["ResID"] = "voice_2_4",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 150, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "高傲跌倒在裙摆下，我愿永世为臣。"
	},
	[151] = {
		["ID"] = 151,
		["ResID"] = "voice_2_7",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 151, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "就当是进入爱丽丝的梦游仙境，把手给我。"
	},
	[152] = {
		["ID"] = 152,
		["ResID"] = "voice_2_8",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 152, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "任何弹奏《梦幻曲》的人都会变成孩子，童年一幕幕在指尖播放。"
	},
	[153] = {
		["ID"] = 153,
		["ResID"] = "voice_2_9",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 153, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "我在迷雾里迷了路，却有幸碰见了你。"
	},
	[154] = {
		["ID"] = 154,
		["ResID"] = "voice_2_12",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 154, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "水底映出的，是欲望，还是本能？"
	},
	[155] = {
		["ID"] = 155,
		["ResID"] = "voice_2_13",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 155, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "不论发生什么，星光一直都在。"
	},
	[156] = {
		["ID"] = 156,
		["ResID"] = "voice_2_14",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 156, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "善恶、黑白、正反……你靠近一点，我来告诉你答案。"
	},
	[157] = {
		["ID"] = 157,
		["ResID"] = "voice_2_17",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 157, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "你见过莫比乌斯环吗？那里藏着我为了遇见你所做的全部努力。"
	},
	[158] = {
		["ID"] = 158,
		["ResID"] = "voice_2_102",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 158, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "即便知道结局，我也会选择开始。"
	},
	[159] = {
		["ID"] = 159,
		["ResID"] = "voice_2_19",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 159, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "现在我们所经历的这一切，开启了未来，也代表着过去。"
	},
	[160] = {
		["ID"] = 160,
		["ResID"] = "voice_2_22",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 160, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "笼中鸟就算自由了，也会狠狠地摔落到地上，留下一地残羽。"
	},
	[161] = {
		["ID"] = 161,
		["ResID"] = "voice_2_23",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 161, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "我飞不上天堂，还坠不了地狱吗？"
	},
	[162] = {
		["ID"] = 162,
		["ResID"] = "voice_2_24",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 162, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "再精确的导航也矫正不对我的方向，我偏心你。"
	},
	[163] = {
		["ID"] = 163,
		["ResID"] = "voice_2_27",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 163, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "后台等我，唱完这首歌一起回家。"
	},
	[164] = {
		["ID"] = 164,
		["ResID"] = "voice_2_28",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 164, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "舞台上的聚光灯是两千瓦，比不上你眼眸里的亮光闪烁。"
	},
	[165] = {
		["ID"] = 165,
		["ResID"] = "voice_2_29",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 165, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "你是贝斯的弦，钢琴的黑白键，音符赖以为生的五线谱。"
	},
	[166] = {
		["ID"] = 166,
		["ResID"] = "voice_2_32",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 166, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "喜欢世界上一切鲜艳的东西……哪怕它们不再鲜活。"
	},
	[167] = {
		["ID"] = 167,
		["ResID"] = "voice_2_34",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 167, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "你知道吗？恶魔的另一个身份也可以是世界上最伟大的画家。"
	},
	[168] = {
		["ID"] = 168,
		["ResID"] = "voice_2_35",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 168, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "舟形乌头的花语是“恶意”，但我对你，从来没有恶意。"
	},
	[169] = {
		["ID"] = 169,
		["ResID"] = "voice_2_37",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 169, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "昨天晚上，我托一只野猫去告诉你，我很想念你。你有没有听到啊？"
	},
	[170] = {
		["ID"] = 170,
		["ResID"] = "voice_2_38",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 170, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "今晚月色真好，适合跟我恋爱。"
	},
	[171] = {
		["ID"] = 171,
		["ResID"] = "voice_2_101",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 171, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "刚刚路过的吸血鬼多看了你一眼，好讨厌。"
	},
	[172] = {
		["ID"] = 172,
		["ResID"] = "voice_2_42",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 172, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "柠檬精？就是……柠檬味的洗洁精吗？"
	},
	[173] = {
		["ID"] = 173,
		["ResID"] = "voice_2_43",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 173, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "酸不酸你尝一口不就知道了。"
	},
	[174] = {
		["ID"] = 174,
		["ResID"] = "voice_2_46",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 174, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "曾有过选择吗？我记不清了。如果……我会选择带你离开。"
	},
	[175] = {
		["ID"] = 175,
		["ResID"] = "voice_2_47",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 175, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "没有人愿意永远站在阴影里，但在你之前，我从未感受过阳光。"
	},
	[176] = {
		["ID"] = 176,
		["ResID"] = "voice_2_50",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 176, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "你会记得我吗？那个你看不见的、真正的我。"
	},
	[177] = {
		["ID"] = 177,
		["ResID"] = "voice_2_51",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 177, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "神明的旨意并不通过实体的介质传播，想听的话，我来说。"
	},
	[178] = {
		["ID"] = 178,
		["ResID"] = "voice_2_54",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 178, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "嘘，看着我，不要说话。"
	},
	[179] = {
		["ID"] = 179,
		["ResID"] = "voice_2_55",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 179, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "剪刀给你，布给你，我的手也给你，这样每次的游戏你都能赢。"
	},
	[180] = {
		["ID"] = 180,
		["ResID"] = "voice_2_58",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 180, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "容易消逝的，往往也是最绚烂的，永远比不上此刻。"
	},
	[181] = {
		["ID"] = 181,
		["ResID"] = "voice_2_59",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 181, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "折射在镜中的花火，又多了几点星光。"
	},
	[182] = {
		["ID"] = 182,
		["ResID"] = "voice_2_62",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 182, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "昨晚梦见我们吵架了，哄了你好久，一直到哄好了才敢醒过来。"
	},
	[183] = {
		["ID"] = 183,
		["ResID"] = "voice_2_63",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 183, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "早餐想吃什么，中式的、西式的、还是肖睿阳独家烹饪的？"
	},
	[184] = {
		["ID"] = 184,
		["ResID"] = "voice_2_66",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 184, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "这是道具玫瑰！我可是只给你一个女生送过花……除了我妈。"
	},
	[185] = {
		["ID"] = 185,
		["ResID"] = "voice_2_67",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 185, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "坦然面对闪光灯，这可是偶像的日常！"
	},
	[186] = {
		["ID"] = 186,
		["ResID"] = "voice_2_74",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 186, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "快过来，别被粉丝发现了。"
	},
	[187] = {
		["ID"] = 187,
		["ResID"] = "voice_2_77",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 187, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "记得给我打电话。"
	},
	[188] = {
		["ID"] = 188,
		["ResID"] = "voice_2_80",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 188, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "我们走吧。"
	},
	[189] = {
		["ID"] = 189,
		["ResID"] = "voice_2_83",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 189, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "我出新歌了。"
	},
	[190] = {
		["ID"] = 190,
		["ResID"] = "voice_2_87",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 190, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "这个女人怎么又生气了。"
	},
	[191] = {
		["ID"] = 191,
		["ResID"] = "voice_2_100",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 191, "Amount": 1}]}]==],
		["Name"] = "语音",
		["Character"] = 2,
		["Desc"] = "这个女人怎么又生气了。"
	}
}