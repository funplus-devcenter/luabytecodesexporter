return {
	[1] = {
		["ID"] = 1,
		["PicID"] = 	{
			"Txt_CardLine1_1",
			"Txt_CardLine1_2",
			"Txt_CardLine1_3"
		},
		["WordID"] = 	{
			"Txt_CardWord1_1",
			"Txt_CardWord1_2",
			"Txt_CardWord1_3",
			"Txt_CardWord1_4",
			"Txt_CardWord1_5"
		},
		["Position"] = {445,-189}
	},
	[2] = {
		["ID"] = 2,
		["PicID"] = 	{
			"Txt_CardLine2_1",
			"Txt_CardLine2_2",
			"Txt_CardLine2_3"
		},
		["WordID"] = 	{
			"Txt_CardWord2_1",
			"Txt_CardWord2_2",
			"Txt_CardWord2_3",
			"Txt_CardWord2_4",
			"Txt_CardWord2_5"
		},
		["Position"] = {445,180}
	},
	[3] = {
		["ID"] = 3,
		["PicID"] = 	{
			"Txt_CardLine3_1",
			"Txt_CardLine3_2",
			"Txt_CardLine3_3"
		},
		["WordID"] = 	{
			"Txt_CardWord3_1",
			"Txt_CardWord3_2",
			"Txt_CardWord3_3",
			"Txt_CardWord3_4",
			"Txt_CardWord3_5"
		},
		["Position"] = {556,36}
	}
}