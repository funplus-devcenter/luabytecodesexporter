return {
	[100100] = {
		["ID"] = 100100,
		["Name"] = "三幕曲",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 5}]}]==],
		["Type"] = 1,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","story_11"},
		["ResBG"] = "BG11"
	},
	[100200] = {
		["ID"] = 100200,
		["Name"] = "未来图书馆",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 5}]}]==],
		["Type"] = 1,
		["ParentNode"] = 100100,
		["Res"] = {"Chapter1","story_15"},
		["ResBG"] = "BG15"
	},
	[100300] = {
		["ID"] = 100300,
		["Name"] = "我的生活",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}]}]==],
		["Type"] = 1,
		["ParentNode"] = 100200,
		["Res"] = {"Chapter1","story_12"},
		["ResBG"] = "BG12"
	},
	[100400] = {
		["ID"] = 100400,
		["Name"] = "发布会",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}]}]==],
		["Type"] = 1,
		["ParentNode"] = 100300,
		["Res"] = {"Chapter1","story_14"},
		["ResBG"] = "BG14"
	},
	[100401] = {
		["ID"] = 100401,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 100400,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[100500] = {
		["ID"] = 100500,
		["Name"] = "发布会闹剧",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}]}]==],
		["Type"] = 1,
		["ParentNode"] = 100400,
		["Res"] = {"Chapter1","story_16"},
		["ResBG"] = "BG16"
	},
	[100501] = {
		["ID"] = 100501,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 100500,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[100502] = {
		["ID"] = 100502,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 100500,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[100600] = {
		["ID"] = 100600,
		["Name"] = "信念动摇",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 15}]}]==],
		["Type"] = 1,
		["ParentNode"] = 100500,
		["Res"] = {"Chapter1","story_17"},
		["ResBG"] = "BG17"
	},
	[100601] = {
		["ID"] = 100601,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 100600,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[100602] = {
		["ID"] = 100602,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 100600,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[100603] = {
		["ID"] = 100603,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 100600,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[100700] = {
		["ID"] = 100700,
		["Name"] = "他回来了",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 15}]}]==],
		["Type"] = 1,
		["ParentNode"] = 100600,
		["Res"] = {"Chapter1","story_13"},
		["ResBG"] = "BG13"
	},
	[100701] = {
		["ID"] = 100701,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 100700,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[100800] = {
		["ID"] = 100800,
		["Name"] = "青葱回忆",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 15}]}]==],
		["Type"] = 1,
		["ParentNode"] = 100700,
		["Res"] = {"Chapter1","story_1"},
		["ResBG"] = "BG1"
	},
	[100900] = {
		["ID"] = 100900,
		["Name"] = "沉眠时光",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 15}]}]==],
		["Type"] = 1,
		["ParentNode"] = 100800,
		["Res"] = {"Chapter1","story_2"},
		["ResBG"] = "BG2"
	},
	[101000] = {
		["ID"] = 101000,
		["Name"] = "偶像的邀约",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 15}]}]==],
		["Type"] = 1,
		["ParentNode"] = 100900,
		["Res"] = {"Chapter1","story_3"},
		["ResBG"] = "BG3"
	},
	[101001] = {
		["ID"] = 101001,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 101000,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[101100] = {
		["ID"] = 101100,
		["Name"] = "绯闻女友",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 20}]}]==],
		["Type"] = 1,
		["ParentNode"] = 101000,
		["Res"] = {"Chapter1","story_5"},
		["ResBG"] = "BG5"
	},
	[101200] = {
		["ID"] = 101200,
		["Name"] = "来自书里的人",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 20}]}]==],
		["Type"] = 1,
		["ParentNode"] = 101100,
		["Res"] = {"Chapter1","story_18"},
		["ResBG"] = "BG18"
	},
	[101201] = {
		["ID"] = 101201,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 101200,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[101300] = {
		["ID"] = 101300,
		["Name"] = "奇怪来信",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 20}]}]==],
		["Type"] = 1,
		["ParentNode"] = 101200,
		["Res"] = {"Chapter1","story_6"},
		["ResBG"] = "BG6"
	},
	[101301] = {
		["ID"] = 101301,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 101300,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[101400] = {
		["ID"] = 101400,
		["Name"] = "初遇督军",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 20}]}]==],
		["Type"] = 1,
		["ParentNode"] = 101300,
		["Res"] = {"Chapter1","story_7"},
		["ResBG"] = "BG7"
	},
	[101401] = {
		["ID"] = 101401,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 101400,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[101500] = {
		["ID"] = 101500,
		["Name"] = "君年之死",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 20}]}]==],
		["Type"] = 1,
		["ParentNode"] = 101400,
		["Res"] = {"Chapter1","story_8"},
		["ResBG"] = "BG8"
	},
	[101501] = {
		["ID"] = 101501,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 101500,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[101502] = {
		["ID"] = 101502,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 101500,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[101503] = {
		["ID"] = 101503,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 101500,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[101600] = {
		["ID"] = 101600,
		["Name"] = "神秘木盒",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 20}]}]==],
		["Type"] = 1,
		["ParentNode"] = 101500,
		["Res"] = {"Chapter1","story_9"},
		["ResBG"] = "BG9"
	},
	[101601] = {
		["ID"] = 101601,
		["Name"] = "回到现实",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 20}]}]==],
		["Type"] = 1,
		["ParentNode"] = 101600,
		["Res"] = {"Chapter1","story_20"},
		["ResBG"] = "BG20"
	},
	[101700] = {
		["ID"] = 101700,
		["Name"] = "最后一舞",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 20}]}]==],
		["Type"] = 1,
		["ParentNode"] = 101600,
		["Res"] = {"Chapter1","story_10"},
		["ResBG"] = "BG10"
	},
	[101701] = {
		["ID"] = 101701,
		["Name"] = "BadEnding",
		["Consume"] = "{}",
		["Type"] = 2,
		["ParentNode"] = 101700,
		["Res"] = {"Chapter1","fail"},
		["ResBG"] = ""
	},
	[300001] = {
		["ID"] = 300001,
		["Name"] = "出道梦想",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 3,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[300002] = {
		["ID"] = 300002,
		["Name"] = "舞台事故",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 3,
		["ParentNode"] = 300001,
		["Res"] = {"Chapter1","Icon_2"},
		["ResBG"] = ""
	},
	[300003] = {
		["ID"] = 300003,
		["Name"] = "第一个粉丝",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 3,
		["ParentNode"] = 300002,
		["Res"] = {"Chapter1","Icon_3"},
		["ResBG"] = ""
	},
	[300004] = {
		["ID"] = 300004,
		["Name"] = "假公济私",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 3,
		["ParentNode"] = 300003,
		["Res"] = {"Chapter1","Icon_4"},
		["ResBG"] = ""
	},
	[300005] = {
		["ID"] = 300005,
		["Name"] = "3G冲浪",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 3,
		["ParentNode"] = 300004,
		["Res"] = {"Chapter1","Icon_5"},
		["ResBG"] = ""
	},
	[300006] = {
		["ID"] = 300006,
		["Name"] = "消失的时间",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 3,
		["ParentNode"] = 300005,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[300007] = {
		["ID"] = 300007,
		["Name"] = "陌生世界",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 3,
		["ParentNode"] = 300006,
		["Res"] = {"Chapter1","Icon_2"},
		["ResBG"] = ""
	},
	[300008] = {
		["ID"] = 300008,
		["Name"] = "酒吧驻唱",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 3,
		["ParentNode"] = 300007,
		["Res"] = {"Chapter1","Icon_3"},
		["ResBG"] = ""
	},
	[300009] = {
		["ID"] = 300009,
		["Name"] = "有趣的她",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 3,
		["ParentNode"] = 300008,
		["Res"] = {"Chapter1","Icon_4"},
		["ResBG"] = ""
	},
	[500001] = {
		["ID"] = 500001,
		["Name"] = "转校生",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 5,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_10"},
		["ResBG"] = ""
	},
	[500002] = {
		["ID"] = 500002,
		["Name"] = "她",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 5,
		["ParentNode"] = 500001,
		["Res"] = {"Chapter1","Icon_11"},
		["ResBG"] = ""
	},
	[500003] = {
		["ID"] = 500003,
		["Name"] = "计划",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 5,
		["ParentNode"] = 500002,
		["Res"] = {"Chapter1","Icon_12"},
		["ResBG"] = ""
	},
	[500004] = {
		["ID"] = 500004,
		["Name"] = "迷迭香",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 5,
		["ParentNode"] = 500003,
		["Res"] = {"Chapter1","Icon_13"},
		["ResBG"] = ""
	},
	[500005] = {
		["ID"] = 500005,
		["Name"] = "落日",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 5,
		["ParentNode"] = 500004,
		["Res"] = {"Chapter1","Icon_14"},
		["ResBG"] = ""
	},
	[500006] = {
		["ID"] = 500006,
		["Name"] = "记忆",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 5,
		["ParentNode"] = 500005,
		["Res"] = {"Chapter1","Icon_10"},
		["ResBG"] = ""
	},
	[500007] = {
		["ID"] = 500007,
		["Name"] = "初恋",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 5,
		["ParentNode"] = 500006,
		["Res"] = {"Chapter1","Icon_11"},
		["ResBG"] = ""
	},
	[500008] = {
		["ID"] = 500008,
		["Name"] = "暴雨",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 5,
		["ParentNode"] = 500007,
		["Res"] = {"Chapter1","Icon_12"},
		["ResBG"] = ""
	},
	[600001] = {
		["ID"] = 600001,
		["Name"] = "月夜",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 6,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_6"},
		["ResBG"] = ""
	},
	[600002] = {
		["ID"] = 600002,
		["Name"] = "小神婆",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 6,
		["ParentNode"] = 600001,
		["Res"] = {"Chapter1","Icon_7"},
		["ResBG"] = ""
	},
	[600003] = {
		["ID"] = 600003,
		["Name"] = "告别",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 6,
		["ParentNode"] = 600002,
		["Res"] = {"Chapter1","Icon_8"},
		["ResBG"] = ""
	},
	[600004] = {
		["ID"] = 600004,
		["Name"] = "重逢",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 6,
		["ParentNode"] = 600003,
		["Res"] = {"Chapter1","Icon_9"},
		["ResBG"] = ""
	},
	[600005] = {
		["ID"] = 600005,
		["Name"] = "木盒",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 6,
		["ParentNode"] = 600004,
		["Res"] = {"Chapter1","Icon_6"},
		["ResBG"] = ""
	},
	[600006] = {
		["ID"] = 600006,
		["Name"] = "约定",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 6,
		["ParentNode"] = 600005,
		["Res"] = {"Chapter1","Icon_7"},
		["ResBG"] = ""
	},
	[600007] = {
		["ID"] = 600007,
		["Name"] = "游戏",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 6,
		["ParentNode"] = 600006,
		["Res"] = {"Chapter1","Icon_8"},
		["ResBG"] = ""
	},
	[600008] = {
		["ID"] = 600008,
		["Name"] = "真心话",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 6,
		["ParentNode"] = 600007,
		["Res"] = {"Chapter1","Icon_9"},
		["ResBG"] = ""
	},
	[600009] = {
		["ID"] = 600009,
		["Name"] = "舞会",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}]}]==],
		["Type"] = 6,
		["ParentNode"] = 600008,
		["Res"] = {"Chapter1","Icon_6"},
		["ResBG"] = ""
	},
	[400001] = {
		["ID"] = 400001,
		["Name"] = "七年",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 201, "Amount": 1}]}]==],
		["Type"] = 4,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_4"},
		["ResBG"] = ""
	},
	[400002] = {
		["ID"] = 400002,
		["Name"] = "皮衣、烟草与猫1",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 202, "Amount": 1}]}]==],
		["Type"] = 4,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[400003] = {
		["ID"] = 400003,
		["Name"] = "鸦啼",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 203, "Amount": 1}]}]==],
		["Type"] = 4,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_2"},
		["ResBG"] = ""
	},
	[400004] = {
		["ID"] = 400004,
		["Name"] = "双飞",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 204, "Amount": 1}]}]==],
		["Type"] = 4,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_3"},
		["ResBG"] = ""
	},
	[700001] = {
		["ID"] = 700001,
		["Name"] = "皮衣、烟草与猫2",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 205, "Amount": 1}]}]==],
		["Type"] = 7,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[800001] = {
		["ID"] = 800001,
		["Name"] = "实验日志1",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 206, "Amount": 1}]}]==],
		["Type"] = 8,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[800002] = {
		["ID"] = 800002,
		["Name"] = "实验日志2",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 207, "Amount": 1}]}]==],
		["Type"] = 8,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[900001] = {
		["ID"] = 900001,
		["Name"] = "好久不见",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 208, "Amount": 1}]}]==],
		["Type"] = 9,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[1000001] = {
		["ID"] = 1000001,
		["Name"] = "猎魔人",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 209, "Amount": 1}]}]==],
		["Type"] = 10,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[1100001] = {
		["ID"] = 1100001,
		["Name"] = "白日酣梦",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 210, "Amount": 1}]}]==],
		["Type"] = 11,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[1200001] = {
		["ID"] = 1200001,
		["Name"] = "完美国度",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 211, "Amount": 1}]}]==],
		["Type"] = 12,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	},
	[1300001] = {
		["ID"] = 1300001,
		["Name"] = "吸血鬼",
		["Consume"] = "{}",
		["Type"] = 13,
		["ParentNode"] = 0,
		["Res"] = {"Chapter1","Icon_1"},
		["ResBG"] = ""
	}
}