return {
	[100101] = {
		["ID"] = 100101,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100101,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100102] = {
		["ID"] = 100102,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100102,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100103] = {
		["ID"] = 100103,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100103,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100104] = {
		["ID"] = 100104,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100104,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100105] = {
		["ID"] = 100105,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100105,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100106] = {
		["ID"] = 100106,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 2,
		["Buff"] = 100106,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100107] = {
		["ID"] = 100107,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100107,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100108] = {
		["ID"] = 100108,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 100108,
		["HitEffect"] = "Fx_CardBuff02",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100109] = {
		["ID"] = 100109,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100109,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100110] = {
		["ID"] = 100110,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100110,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100111] = {
		["ID"] = 100111,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100111,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100112] = {
		["ID"] = 100112,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100112,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100113] = {
		["ID"] = 100113,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100113,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100114] = {
		["ID"] = 100114,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 100114,
		["HitEffect"] = "Fx_CardBuff02",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100115] = {
		["ID"] = 100115,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100115,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100116] = {
		["ID"] = 100116,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 100116,
		["HitEffect"] = "Fx_CardBuff02",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100117] = {
		["ID"] = 100117,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100117,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100118] = {
		["ID"] = 100118,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100118,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100119] = {
		["ID"] = 100119,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100119,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100120] = {
		["ID"] = 100120,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100120,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100121] = {
		["ID"] = 100121,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100121,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100122] = {
		["ID"] = 100122,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100122,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100123] = {
		["ID"] = 100123,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100123,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100124] = {
		["ID"] = 100124,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100124,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100125] = {
		["ID"] = 100125,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100125,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100126] = {
		["ID"] = 100126,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100126,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100127] = {
		["ID"] = 100127,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 3,
		["Buff"] = 100127,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100128] = {
		["ID"] = 100128,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100128,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100129] = {
		["ID"] = 100129,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100129,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100130] = {
		["ID"] = 100130,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100130,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100131] = {
		["ID"] = 100131,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 2,
		["Buff"] = 100131,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100132] = {
		["ID"] = 100132,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100132,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100133] = {
		["ID"] = 100133,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 100133,
		["HitEffect"] = "Fx_CardBuff02",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100134] = {
		["ID"] = 100134,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100134,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100135] = {
		["ID"] = 100135,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100135,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100136] = {
		["ID"] = 100136,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100136,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100137] = {
		["ID"] = 100137,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100137,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100138] = {
		["ID"] = 100138,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100138,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100139] = {
		["ID"] = 100139,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100139,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100140] = {
		["ID"] = 100140,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100140,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100141] = {
		["ID"] = 100141,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100141,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[100142] = {
		["ID"] = 100142,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 2,
		["Buff"] = 100142,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200001] = {
		["ID"] = 200001,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200001,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200002] = {
		["ID"] = 200002,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200002,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200003] = {
		["ID"] = 200003,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200003,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200004] = {
		["ID"] = 200004,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 200004,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200005] = {
		["ID"] = 200005,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200005,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200006] = {
		["ID"] = 200006,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 200006,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200007] = {
		["ID"] = 200007,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200007,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200008] = {
		["ID"] = 200008,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 200008,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200009] = {
		["ID"] = 200009,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200009,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200010] = {
		["ID"] = 200010,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 200010,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200011] = {
		["ID"] = 200011,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200011,
		["HitEffect"] = "Fx_CardBuff03",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200012] = {
		["ID"] = 200012,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 3,
		["Buff"] = 200012,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200013] = {
		["ID"] = 200013,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200013,
		["HitEffect"] = "Fx_CardBuff03",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200014] = {
		["ID"] = 200014,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200014,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200015] = {
		["ID"] = 200015,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 200015,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200016] = {
		["ID"] = 200016,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200016,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200017] = {
		["ID"] = 200017,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 200017,
		["HitEffect"] = "Fx_CardBuff02",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200018] = {
		["ID"] = 200018,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 200018,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[300101] = {
		["ID"] = 300101,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 300101,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[300102] = {
		["ID"] = 300102,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 300102,
		["HitEffect"] = "Fx_CardBuff03",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[300103] = {
		["ID"] = 300103,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 300103,
		["HitEffect"] = "Fx_CardBuff01",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[300104] = {
		["ID"] = 300104,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 2,
		["Buff"] = 300104,
		["HitEffect"] = "Fx_CardBuff03",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[300105] = {
		["ID"] = 300105,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 2,
		["Buff"] = 300105,
		["HitEffect"] = "Fx_CardBuff02",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[300106] = {
		["ID"] = 300106,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 300106,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[300107] = {
		["ID"] = 300107,
		["Desc"] = "",
		["Target"] = 3,
		["TargetRange"] = 3,
		["Buff"] = 300107,
		["HitEffect"] = "Fx_Impact_D",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[200019] = {
		["ID"] = 200019,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 200019,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[200020] = {
		["ID"] = 200020,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 200020,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 3.4,
		["SpecialEffect"] = "Null"
	},
	[400000] = {
		["ID"] = 400000,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 400001,
		["HitEffect"] = "Fx_CardImpact_wuya01_skill",
		["HitEffectDelay"] = 1.2,
		["SpecialEffect"] = "Null"
	},
	[400001] = {
		["ID"] = 400001,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 400001,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 1.4,
		["SpecialEffect"] = "Null"
	},
	[400002] = {
		["ID"] = 400002,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 400001,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 1.6,
		["SpecialEffect"] = "Null"
	},
	[400003] = {
		["ID"] = 400003,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 400001,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 1.8,
		["SpecialEffect"] = "Null"
	},
	[400004] = {
		["ID"] = 400004,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 300106,
		["HitEffect"] = "Fx_CardImpact_wuya01_atk",
		["HitEffectDelay"] = 1.1,
		["SpecialEffect"] = "Null"
	},
	[400005] = {
		["ID"] = 400005,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100103,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[300108] = {
		["ID"] = 300108,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100139,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[300109] = {
		["ID"] = 300109,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 100116,
		["HitEffect"] = "Fx_CardBuff03",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400006] = {
		["ID"] = 400006,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 300101,
		["HitEffect"] = "Fx_CardImpact_wuya02_atk",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400007] = {
		["ID"] = 400007,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 300101,
		["HitEffect"] = "Fx_CardImpact_wuya02_atk",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400008] = {
		["ID"] = 400008,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100139,
		["HitEffect"] = "Fx_CardImpact_wuya02_atk",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400009] = {
		["ID"] = 400009,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100138,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400010] = {
		["ID"] = 400010,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100126,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400011] = {
		["ID"] = 400011,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200011,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400012] = {
		["ID"] = 400012,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100120,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400013] = {
		["ID"] = 400013,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100132,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400014] = {
		["ID"] = 400014,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 200015,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400015] = {
		["ID"] = 400015,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100134,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400016] = {
		["ID"] = 400016,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200016,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400017] = {
		["ID"] = 400017,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100107,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400018] = {
		["ID"] = 400018,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200002,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400019] = {
		["ID"] = 400019,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100102,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400020] = {
		["ID"] = 400020,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 100114,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400021] = {
		["ID"] = 400021,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100111,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400022] = {
		["ID"] = 400022,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 1,
		["Buff"] = 100135,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400023] = {
		["ID"] = 400023,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100112,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400024] = {
		["ID"] = 400024,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 200006,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400025] = {
		["ID"] = 400025,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 100133,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400026] = {
		["ID"] = 400026,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100110,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400027] = {
		["ID"] = 400027,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200005,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400028] = {
		["ID"] = 400028,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100124,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400029] = {
		["ID"] = 400029,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 100113,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400030] = {
		["ID"] = 400030,
		["Desc"] = "",
		["Target"] = 2,
		["TargetRange"] = 3,
		["Buff"] = 200007,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400031] = {
		["ID"] = 400031,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100112,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400032] = {
		["ID"] = 400032,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 1,
		["Buff"] = 200006,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	},
	[400033] = {
		["ID"] = 400033,
		["Desc"] = "",
		["Target"] = 1,
		["TargetRange"] = 2,
		["Buff"] = 100103,
		["HitEffect"] = "Null",
		["HitEffectDelay"] = 0.5,
		["SpecialEffect"] = "Null"
	}
}