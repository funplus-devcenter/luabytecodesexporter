return {
	[101001] = {
		["ID"] = 101001,
		["Name"] = "1-1 出走鸦灵①",
		["Desc"] = "忠心的鸦灵为何消失？",
		["Res"] = "Battle_1",
		["ChapterID"] = 101,
		["NextStageID"] = 101002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 22, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100001,100001,100001}
			},
					{
				["value"] = {100002,100001,100002}
			},
					{
				["value"] = {100002,100003,100001}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[101002] = {
		["ID"] = 101002,
		["Name"] = "1-2 出走鸦灵②",
		["Desc"] = "忠心的鸦灵为何消失？",
		["Res"] = "Battle_2",
		["ChapterID"] = 101,
		["NextStageID"] = 101003,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 23, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100004,100004,100004}
			},
					{
				["value"] = {100005,100004,100005}
			},
					{
				["value"] = {100005,100006,100004}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[101003] = {
		["ID"] = 101003,
		["Name"] = "1-3 出走鸦灵③",
		["Desc"] = "忠心的鸦灵为何消失？",
		["Res"] = "Battle_3",
		["ChapterID"] = 101,
		["NextStageID"] = 101004,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 24, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100007,100007,100007}
			},
					{
				["value"] = {100008,100007,100008}
			},
					{
				["value"] = {100008,100009,100007}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[101004] = {
		["ID"] = 101004,
		["Name"] = "1-4 破碎水晶①",
		["Desc"] = "巫杜莎的水晶球碎片掉了一地。",
		["Res"] = "Battle_4",
		["ChapterID"] = 101,
		["NextStageID"] = 101005,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 25, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100010,100010,100010}
			},
					{
				["value"] = {100011,100010,100011}
			},
					{
				["value"] = {100011,100012,100010}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[101005] = {
		["ID"] = 101005,
		["Name"] = "1-5 破碎水晶②",
		["Desc"] = "巫杜莎的水晶球碎片掉了一地。",
		["Res"] = "Battle_1",
		["ChapterID"] = 101,
		["NextStageID"] = 102001,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 26, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100004,100013,100004}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102001] = {
		["ID"] = 102001,
		["Name"] = "2-1 不毛之地①",
		["Desc"] = "不是说森林吗？树呢。",
		["Res"] = "Battle_2",
		["ChapterID"] = 102,
		["NextStageID"] = 102002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 22, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100016,100016,100016}
			},
					{
				["value"] = {100017,100016,100017}
			},
					{
				["value"] = {100017,100018,100016}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102002] = {
		["ID"] = 102002,
		["Name"] = "2-2 不毛之地②",
		["Desc"] = "不是说森林吗？树呢。",
		["Res"] = "Battle_3",
		["ChapterID"] = 102,
		["NextStageID"] = 102003,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 23, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100019,100019,100019}
			},
					{
				["value"] = {100020,100019,100020}
			},
					{
				["value"] = {100020,100021,100019}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102003] = {
		["ID"] = 102003,
		["Name"] = "2-3 不毛之地③",
		["Desc"] = "不是说森林吗？树呢。",
		["Res"] = "Battle_4",
		["ChapterID"] = 102,
		["NextStageID"] = 102004,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 24, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100022,100022,100022}
			},
					{
				["value"] = {100023,100022,100023}
			},
					{
				["value"] = {100023,100024,100022}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102004] = {
		["ID"] = 102004,
		["Name"] = "2-4 寻找墨西①",
		["Desc"] = "墨西是谁，或者说它在哪？",
		["Res"] = "Battle_1",
		["ChapterID"] = 102,
		["NextStageID"] = 102005,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 25, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100025,100025,100025}
			},
					{
				["value"] = {100026,100025,100026}
			},
					{
				["value"] = {100026,100027,100025}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102005] = {
		["ID"] = 102005,
		["Name"] = "2-5 寻找墨西②",
		["Desc"] = "墨西是谁，或者说它在哪？",
		["Res"] = "Battle_2",
		["ChapterID"] = 102,
		["NextStageID"] = 102006,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 26, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100028,100028,100028}
			},
					{
				["value"] = {100029,100028,100029}
			},
					{
				["value"] = {100029,100030,100028}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102006] = {
		["ID"] = 102006,
		["Name"] = "2-6 寻找墨西③",
		["Desc"] = "墨西是谁，或者说它在哪？",
		["Res"] = "Battle_3",
		["ChapterID"] = 102,
		["NextStageID"] = 102007,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 22, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100028,100031,100029}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102007] = {
		["ID"] = 102007,
		["Name"] = "2-7 寻找墨西④",
		["Desc"] = "墨西是谁，或者说它在哪？",
		["Res"] = "Battle_4",
		["ChapterID"] = 102,
		["NextStageID"] = 102008,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 23, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100034,100034,100034}
			},
					{
				["value"] = {100035,100034,100035}
			},
					{
				["value"] = {100035,100036,100034}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102008] = {
		["ID"] = 102008,
		["Name"] = "2-8 杀死恶龙①",
		["Desc"] = "下一个墨西与新的勇士。",
		["Res"] = "Battle_1",
		["ChapterID"] = 102,
		["NextStageID"] = 102009,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 24, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100037,100037,100037}
			},
					{
				["value"] = {100038,100037,100038}
			},
					{
				["value"] = {100038,100039,100037}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102009] = {
		["ID"] = 102009,
		["Name"] = "2-9 杀死恶龙②",
		["Desc"] = "下一个墨西与新的勇士。",
		["Res"] = "Battle_2",
		["ChapterID"] = 102,
		["NextStageID"] = 102010,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 25, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100040,100040,100040}
			},
					{
				["value"] = {100041,100040,100041}
			},
					{
				["value"] = {100041,100042,100040}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[102010] = {
		["ID"] = 102010,
		["Name"] = "2-10 杀死恶龙③",
		["Desc"] = "下一个墨西与新的勇士。",
		["Res"] = "Battle_3",
		["ChapterID"] = 102,
		["NextStageID"] = 103001,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 26, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100040,100043,100040}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103001] = {
		["ID"] = 103001,
		["Name"] = "3-1 失窃的小鱼干①",
		["Desc"] = "用来喂猫的小鱼干竟然被偷走了。",
		["Res"] = "Battle_4",
		["ChapterID"] = 103,
		["NextStageID"] = 103002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 27, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100046,100046,100046}
			},
					{
				["value"] = {100047,100046,100047}
			},
					{
				["value"] = {100047,100048,100046}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103002] = {
		["ID"] = 103002,
		["Name"] = "3-2 失窃的小鱼干②",
		["Desc"] = "用来喂猫的小鱼干竟然被偷走了。",
		["Res"] = "Battle_1",
		["ChapterID"] = 103,
		["NextStageID"] = 103003,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 28, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100049,100049,100049}
			},
					{
				["value"] = {100050,100049,100050}
			},
					{
				["value"] = {100050,100051,100049}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103003] = {
		["ID"] = 103003,
		["Name"] = "3-3 失窃的小鱼干③",
		["Desc"] = "用来喂猫的小鱼干竟然被偷走了。",
		["Res"] = "Battle_2",
		["ChapterID"] = 103,
		["NextStageID"] = 103004,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 29, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100052,100052,100052}
			},
					{
				["value"] = {100053,100052,100053}
			},
					{
				["value"] = {100053,100054,100052}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103004] = {
		["ID"] = 103004,
		["Name"] = "3-4 猫罐头告急①",
		["Desc"] = "猫咪越来越多，可罐头竟然不够了……",
		["Res"] = "Battle_3",
		["ChapterID"] = 103,
		["NextStageID"] = 103005,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 30, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100055,100055,100055}
			},
					{
				["value"] = {100056,100055,100056}
			},
					{
				["value"] = {100056,100057,100055}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103005] = {
		["ID"] = 103005,
		["Name"] = "3-5 猫罐头告急②",
		["Desc"] = "猫咪越来越多，可罐头竟然不够了……",
		["Res"] = "Battle_4",
		["ChapterID"] = 103,
		["NextStageID"] = 103006,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 31, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100058,100058,100058}
			},
					{
				["value"] = {100059,100058,100059}
			},
					{
				["value"] = {100059,100060,100058}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103006] = {
		["ID"] = 103006,
		["Name"] = "3-6 猫罐头告急③",
		["Desc"] = "猫咪越来越多，可罐头竟然不够了……",
		["Res"] = "Battle_1",
		["ChapterID"] = 103,
		["NextStageID"] = 103007,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 27, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100061,100061,100061}
			},
					{
				["value"] = {100062,100061,100062}
			},
					{
				["value"] = {100062,100063,100061}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103007] = {
		["ID"] = 103007,
		["Name"] = "3-7 混乱毛线球①",
		["Desc"] = "圆滚滚的毛线球究竟被谁扯乱了？",
		["Res"] = "Battle_2",
		["ChapterID"] = 103,
		["NextStageID"] = 103008,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 28, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100064,100064,100064}
			},
					{
				["value"] = {100065,100064,100065}
			},
					{
				["value"] = {100065,100066,100064}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103008] = {
		["ID"] = 103008,
		["Name"] = "3-8 混乱毛线球②",
		["Desc"] = "圆滚滚的毛线球究竟被谁扯乱了？",
		["Res"] = "Battle_3",
		["ChapterID"] = 103,
		["NextStageID"] = 103009,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 29, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100067,100067,100067}
			},
					{
				["value"] = {100068,100067,100068}
			},
					{
				["value"] = {100068,100069,100067}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103009] = {
		["ID"] = 103009,
		["Name"] = "3-9 混乱毛线球③",
		["Desc"] = "圆滚滚的毛线球究竟被谁扯乱了？",
		["Res"] = "Battle_4",
		["ChapterID"] = 103,
		["NextStageID"] = 103010,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 30, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100070,100070,100070}
			},
					{
				["value"] = {100071,100070,100071}
			},
					{
				["value"] = {100071,100072,100070}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[103010] = {
		["ID"] = 103010,
		["Name"] = "3-10 混乱毛线球④",
		["Desc"] = "圆滚滚的毛线球究竟被谁扯乱了？",
		["Res"] = "Battle_1",
		["ChapterID"] = 103,
		["NextStageID"] = 104001,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 31, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100070,100073,100071}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104001] = {
		["ID"] = 104001,
		["Name"] = "4-1 时空之门①",
		["Desc"] = "救急，书里的穿越设备突然失灵了。",
		["Res"] = "Battle_2",
		["ChapterID"] = 104,
		["NextStageID"] = 104002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 32, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100076,100076,100076}
			},
					{
				["value"] = {100077,100076,100077}
			},
					{
				["value"] = {100077,100078,100076}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104002] = {
		["ID"] = 104002,
		["Name"] = "4-2 时空之门②",
		["Desc"] = "救急，书里的穿越设备突然失灵了。",
		["Res"] = "Battle_3",
		["ChapterID"] = 104,
		["NextStageID"] = 104003,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 33, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100079,100079,100079}
			},
					{
				["value"] = {100080,100079,100080}
			},
					{
				["value"] = {100080,100081,100079}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104003] = {
		["ID"] = 104003,
		["Name"] = "4-3 时空之门③",
		["Desc"] = "救急，书里的穿越设备突然失灵了。",
		["Res"] = "Battle_4",
		["ChapterID"] = 104,
		["NextStageID"] = 104004,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 34, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100082,100082,100082}
			},
					{
				["value"] = {100083,100082,100083}
			},
					{
				["value"] = {100083,100084,100082}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104004] = {
		["ID"] = 104004,
		["Name"] = "4-4 尼罗之恋①",
		["Desc"] = "恋爱场景里的BGM不见啦！",
		["Res"] = "Battle_1",
		["ChapterID"] = 104,
		["NextStageID"] = 104005,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 35, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100085,100085,100085}
			},
					{
				["value"] = {100086,100085,100086}
			},
					{
				["value"] = {100086,100087,100085}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104005] = {
		["ID"] = 104005,
		["Name"] = "4-5 尼罗之恋②",
		["Desc"] = "恋爱场景里的BGM不见啦！",
		["Res"] = "Battle_2",
		["ChapterID"] = 104,
		["NextStageID"] = 104006,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 36, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100088,100088,100088}
			},
					{
				["value"] = {100089,100088,100089}
			},
					{
				["value"] = {100089,100090,100088}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104006] = {
		["ID"] = 104006,
		["Name"] = "4-6 尼罗之恋③",
		["Desc"] = "恋爱场景里的BGM不见啦！",
		["Res"] = "Battle_3",
		["ChapterID"] = 104,
		["NextStageID"] = 104007,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 32, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100091,100091,100091}
			},
					{
				["value"] = {100092,100091,100092}
			},
					{
				["value"] = {100092,100093,100091}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104007] = {
		["ID"] = 104007,
		["Name"] = "4-7 权杖之争①",
		["Desc"] = "重要的道具权杖怎么会遗失呢？",
		["Res"] = "Battle_4",
		["ChapterID"] = 104,
		["NextStageID"] = 104008,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 33, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100094,100094,100094}
			},
					{
				["value"] = {100095,100094,100095}
			},
					{
				["value"] = {100095,100096,100094}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104008] = {
		["ID"] = 104008,
		["Name"] = "4-8 权杖之争②",
		["Desc"] = "重要的道具权杖怎么会遗失呢？",
		["Res"] = "Battle_1",
		["ChapterID"] = 104,
		["NextStageID"] = 104009,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 34, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100097,100097,100097}
			},
					{
				["value"] = {100098,100097,100098}
			},
					{
				["value"] = {100098,100099,100097}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104009] = {
		["ID"] = 104009,
		["Name"] = "4-9 权杖之争③",
		["Desc"] = "重要的道具权杖怎么会遗失呢？",
		["Res"] = "Battle_2",
		["ChapterID"] = 104,
		["NextStageID"] = 104010,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 35, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100100,100100,100100}
			},
					{
				["value"] = {100101,100100,100101}
			},
					{
				["value"] = {100101,100102,100100}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[104010] = {
		["ID"] = 104010,
		["Name"] = "4-10 权杖之争④",
		["Desc"] = "重要的道具权杖怎么会遗失呢？",
		["Res"] = "Battle_3",
		["ChapterID"] = 104,
		["NextStageID"] = 0,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 6, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = [==[{"RandomResources": [{"Type": "ITEM", "Id": 36, "Amount": 1, "Weight": 500}], "EmptyWeight": 500}]==],
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100004,100103,100005}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[201001] = {
		["ID"] = 201001,
		["Name"] = "困难 1-1 出走鸦灵①",
		["Desc"] = "忠心的鸦灵为何消失？",
		["Res"] = "Battle_4",
		["ChapterID"] = 201,
		["NextStageID"] = 201002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100013,100104,100072}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[201002] = {
		["ID"] = 201002,
		["Name"] = "困难 1-2 出走鸦灵②",
		["Desc"] = "忠心的鸦灵为何消失？",
		["Res"] = "Battle_1",
		["ChapterID"] = 201,
		["NextStageID"] = 201003,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100016,100105,100075}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[201003] = {
		["ID"] = 201003,
		["Name"] = "困难 1-3 出走鸦灵③",
		["Desc"] = "忠心的鸦灵为何消失？",
		["Res"] = "Battle_2",
		["ChapterID"] = 201,
		["NextStageID"] = 201004,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100019,100106,100078}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[201004] = {
		["ID"] = 201004,
		["Name"] = "困难 1-4 破碎水晶①",
		["Desc"] = "巫杜莎的水晶球碎片掉了一地。",
		["Res"] = "Battle_3",
		["ChapterID"] = 201,
		["NextStageID"] = 201005,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100022,100107,100081}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[201005] = {
		["ID"] = 201005,
		["Name"] = "困难 1-5 破碎水晶②",
		["Desc"] = "巫杜莎的水晶球碎片掉了一地。",
		["Res"] = "Battle_4",
		["ChapterID"] = 201,
		["NextStageID"] = 202001,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100025,100108,100084}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[202001] = {
		["ID"] = 202001,
		["Name"] = "困难 2-1 不毛之地①",
		["Desc"] = "不是说森林吗？树呢。",
		["Res"] = "Battle_1",
		["ChapterID"] = 202,
		["NextStageID"] = 202002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100028,100109,100087}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[202002] = {
		["ID"] = 202002,
		["Name"] = "困难 2-2 不毛之地②",
		["Desc"] = "不是说森林吗？树呢。",
		["Res"] = "Battle_2",
		["ChapterID"] = 202,
		["NextStageID"] = 202003,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100031,100110,100090}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[202003] = {
		["ID"] = 202003,
		["Name"] = "困难 2-3 不毛之地③",
		["Desc"] = "不是说森林吗？树呢。",
		["Res"] = "Battle_3",
		["ChapterID"] = 202,
		["NextStageID"] = 202004,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100034,100111,100093}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[202004] = {
		["ID"] = 202004,
		["Name"] = "困难 2-4 寻找墨西①",
		["Desc"] = "墨西是谁，或者说它在哪？",
		["Res"] = "Battle_4",
		["ChapterID"] = 202,
		["NextStageID"] = 202005,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100037,100112,100096}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[202005] = {
		["ID"] = 202005,
		["Name"] = "困难 2-5 寻找墨西②",
		["Desc"] = "墨西是谁，或者说它在哪？",
		["Res"] = "Battle_1",
		["ChapterID"] = 202,
		["NextStageID"] = 203001,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100040,100113,100026}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[203001] = {
		["ID"] = 203001,
		["Name"] = "困难 3-1 失窃的小鱼干①",
		["Desc"] = "用来喂猫的小鱼干竟然被偷走了。",
		["Res"] = "Battle_2",
		["ChapterID"] = 203,
		["NextStageID"] = 203002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100043,100114,100029}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[203002] = {
		["ID"] = 203002,
		["Name"] = "困难 3-2 失窃的小鱼干②",
		["Desc"] = "用来喂猫的小鱼干竟然被偷走了。",
		["Res"] = "Battle_3",
		["ChapterID"] = 203,
		["NextStageID"] = 203003,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100046,100115,100032}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[203003] = {
		["ID"] = 203003,
		["Name"] = "困难 3-3 失窃的小鱼干③",
		["Desc"] = "用来喂猫的小鱼干竟然被偷走了。",
		["Res"] = "Battle_4",
		["ChapterID"] = 203,
		["NextStageID"] = 203004,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100049,100116,100035}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[203004] = {
		["ID"] = 203004,
		["Name"] = "困难 3-4 猫罐头告急①",
		["Desc"] = "猫咪越来越多，可罐头竟然不够了……",
		["Res"] = "Battle_1",
		["ChapterID"] = 203,
		["NextStageID"] = 203005,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100052,100117,100038}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[203005] = {
		["ID"] = 203005,
		["Name"] = "困难 3-5 猫罐头告急②",
		["Desc"] = "猫咪越来越多，可罐头竟然不够了……",
		["Res"] = "Battle_2",
		["ChapterID"] = 203,
		["NextStageID"] = 204001,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 1000}, {"Type": "ITEM", "Id": 1, "Amount": 1}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100055,100118,100041}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[204001] = {
		["ID"] = 204001,
		["Name"] = "困难 4-1 时空之门①",
		["Desc"] = "救急，书里的穿越设备突然失灵了。",
		["Res"] = "Battle_3",
		["ChapterID"] = 204,
		["NextStageID"] = 204002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 20}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 200}, {"Type": "CURRENCY", "Id": 2, "Amount": 2000}, {"Type": "ITEM", "Id": 1, "Amount": 2}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100058,100119,100044}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[204002] = {
		["ID"] = 204002,
		["Name"] = "困难 4-2 时空之门②",
		["Desc"] = "救急，书里的穿越设备突然失灵了。",
		["Res"] = "Battle_4",
		["ChapterID"] = 204,
		["NextStageID"] = 204003,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 20}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 200}, {"Type": "CURRENCY", "Id": 2, "Amount": 2000}, {"Type": "ITEM", "Id": 1, "Amount": 2}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100061,100120,100047}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[204003] = {
		["ID"] = 204003,
		["Name"] = "困难 4-3 时空之门③",
		["Desc"] = "救急，书里的穿越设备突然失灵了。",
		["Res"] = "Battle_1",
		["ChapterID"] = 204,
		["NextStageID"] = 204004,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 20}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 200}, {"Type": "CURRENCY", "Id": 2, "Amount": 2000}, {"Type": "ITEM", "Id": 1, "Amount": 2}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100064,100121,100050}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[204004] = {
		["ID"] = 204004,
		["Name"] = "困难 4-4 尼罗之恋①",
		["Desc"] = "恋爱场景里的BGM不见啦！",
		["Res"] = "Battle_2",
		["ChapterID"] = 204,
		["NextStageID"] = 204005,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 30}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 300}, {"Type": "CURRENCY", "Id": 2, "Amount": 3000}, {"Type": "ITEM", "Id": 1, "Amount": 3}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100067,100122,100053}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[204005] = {
		["ID"] = 204005,
		["Name"] = "困难 4-5 尼罗之恋②",
		["Desc"] = "恋爱场景里的BGM不见啦！",
		["Res"] = "Battle_3",
		["ChapterID"] = 204,
		["NextStageID"] = 0,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 3,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 30}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 5, "Amount": 10}, {"Type": "CURRENCY", "Id": 3, "Amount": 30}, {"Type": "CURRENCY", "Id": 8, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 300}, {"Type": "CURRENCY", "Id": 2, "Amount": 3000}, {"Type": "ITEM", "Id": 1, "Amount": 3}]}]==],
		["RandomReward"] = "",
		["Wave"] = 1,
		["MonsterID"] = 	{
					{
				["value"] = {100070,100123,100056}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[301001] = {
		["ID"] = 301001,
		["Name"] = "我要金币1",
		["Desc"] = "我要金币1",
		["Res"] = "Battle_1",
		["ChapterID"] = 301,
		["NextStageID"] = 301002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "CURRENCY", "Id": 2, "Amount": 8000}, {"Type": "ITEM", "Id": 1, "Amount": 8}]}]==],
		["RandomReward"] = "",
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100025,100025,100025}
			},
					{
				["value"] = {100026,100025,100026}
			},
					{
				["value"] = {100026,100027,100025}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[301002] = {
		["ID"] = 301002,
		["Name"] = "我要金币2",
		["Desc"] = "我要金币2",
		["Res"] = "Battle_2",
		["ChapterID"] = 301,
		["NextStageID"] = 301003,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 20}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 200}, {"Type": "CURRENCY", "Id": 2, "Amount": 20000}]}]==],
		["RandomReward"] = "",
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100040,100040,100040}
			},
					{
				["value"] = {100041,100040,100041}
			},
					{
				["value"] = {100041,100042,100040}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[301003] = {
		["ID"] = 301003,
		["Name"] = "我要金币3",
		["Desc"] = "我要金币3",
		["Res"] = "Battle_3",
		["ChapterID"] = 301,
		["NextStageID"] = 0,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 30}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 300}, {"Type": "CURRENCY", "Id": 2, "Amount": 36000}]}]==],
		["RandomReward"] = "",
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100070,100070,100070}
			},
					{
				["value"] = {100071,100070,100071}
			},
					{
				["value"] = {100071,100072,100070}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[302001] = {
		["ID"] = 302001,
		["Name"] = "我要经验1",
		["Desc"] = "我要经验1",
		["Res"] = "Battle_1",
		["ChapterID"] = 302,
		["NextStageID"] = 302001,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 10}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 100}, {"Type": "ITEM", "Id": 1, "Amount": 8}]}]==],
		["RandomReward"] = "",
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100028,100028,100028}
			},
					{
				["value"] = {100029,100028,100029}
			},
					{
				["value"] = {100029,100030,100028}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[302002] = {
		["ID"] = 302002,
		["Name"] = "我要经验2",
		["Desc"] = "我要经验2",
		["Res"] = "Battle_2",
		["ChapterID"] = 302,
		["NextStageID"] = 302002,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 20}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 200}, {"Type": "ITEM", "Id": 2, "Amount": 20}]}]==],
		["RandomReward"] = "",
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100058,100058,100058}
			},
					{
				["value"] = {100059,100058,100059}
			},
					{
				["value"] = {100059,100060,100058}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	},
	[302003] = {
		["ID"] = 302003,
		["Name"] = "我要经验3",
		["Desc"] = "我要经验3",
		["Res"] = "Battle_3",
		["ChapterID"] = 302,
		["NextStageID"] = 0,
		["UnlockLevel"] = 1,
		["LimitedTimes"] = 0,
		["StarCondition"] = {1,2,3},
		["WinConsume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 30}]}]==],
		["LoseConsume"] = "{}",
		["FirstReward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 1, "Amount": 300}, {"Type": "ITEM", "Id": 3, "Amount": 10}, {"Type": "ITEM", "Id": 1, "Amount": 6}]}]==],
		["RandomReward"] = "",
		["Wave"] = 3,
		["MonsterID"] = 	{
					{
				["value"] = {100097,100097,100097}
			},
					{
				["value"] = {100098,100097,100098}
			},
					{
				["value"] = {100098,100099,100097}
			}
		},
		["PositionID"] = [==[[[{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}], [{"x": 1, "y": 1}, {"x": 6, "y": 1}, {"x": 11, "y": 1}]]]==]
	}
}