return {
	[1] = {
		["ID"] = 1,
		["Name"] = "第一章",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/ui_chapter_1",
		["ResName"] = "Null",
		["Type"] = 0
	},
	[3] = {
		["ID"] = 3,
		["Name"] = "肖睿阳",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose01",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name01",
		["Type"] = 1
	},
	[4] = {
		["ID"] = 4,
		["Name"] = "顾淮-卡牌·民国",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose06",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name01",
		["Type"] = 1
	},
	[5] = {
		["ID"] = 5,
		["Name"] = "顾淮",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose02",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name02",
		["Type"] = 1
	},
	[6] = {
		["ID"] = 6,
		["Name"] = "傅君年",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose03",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name03",
		["Type"] = 1
	},
	[7] = {
		["ID"] = 7,
		["Name"] = "肖睿阳-卡牌·民国",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose07",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name03",
		["Type"] = 1
	},
	[8] = {
		["ID"] = 8,
		["Name"] = "顾淮-卡牌·血袋",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose05",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name03",
		["Type"] = 1
	},
	[9] = {
		["ID"] = 9,
		["Name"] = "肖睿阳-卡牌·入狱",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose08",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name03",
		["Type"] = 1
	},
	[10] = {
		["ID"] = 10,
		["Name"] = "顾淮-卡牌·假天使",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose09",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name03",
		["Type"] = 1
	},
	[11] = {
		["ID"] = 11,
		["Name"] = "顾淮-卡牌·医生",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose10",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name03",
		["Type"] = 1
	},
	[12] = {
		["ID"] = 12,
		["Name"] = "肖睿阳-卡牌·白羽",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose11",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name03",
		["Type"] = 1
	},
	[13] = {
		["ID"] = 13,
		["Name"] = "顾淮-吸血鬼",
		["Res"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_jiaose11",
		["ResName"] = "UGUI/Sprites/Dynamic/Chapters/GS_im_name03",
		["Type"] = 1
	}
}