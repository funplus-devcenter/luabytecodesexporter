return {
	[1] = {
		["ID"] = 1,
		["LotteryId"] = 1,
		["Weight"] = 400,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100018, "Amount": 1}]}]==]
	},
	[2] = {
		["ID"] = 2,
		["LotteryId"] = 1,
		["Weight"] = 400,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100019, "Amount": 1}]}]==]
	},
	[3] = {
		["ID"] = 3,
		["LotteryId"] = 1,
		["Weight"] = 400,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100020, "Amount": 1}]}]==]
	},
	[4] = {
		["ID"] = 4,
		["LotteryId"] = 1,
		["Weight"] = 400,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100021, "Amount": 1}]}]==]
	},
	[5] = {
		["ID"] = 5,
		["LotteryId"] = 1,
		["Weight"] = 400,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200017, "Amount": 1}]}]==]
	},
	[6] = {
		["ID"] = 6,
		["LotteryId"] = 1,
		["Weight"] = 400,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200018, "Amount": 1}]}]==]
	},
	[7] = {
		["ID"] = 7,
		["LotteryId"] = 1,
		["Weight"] = 400,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200019, "Amount": 1}]}]==]
	},
	[8] = {
		["ID"] = 8,
		["LotteryId"] = 1,
		["Weight"] = 400,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200020, "Amount": 1}]}]==]
	},
	[9] = {
		["ID"] = 9,
		["LotteryId"] = 1,
		["Weight"] = 300,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100017, "Amount": 1}]}]==]
	},
	[10] = {
		["ID"] = 10,
		["LotteryId"] = 1,
		["Weight"] = 300,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200016, "Amount": 1}]}]==]
	},
	[11] = {
		["ID"] = 11,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100012, "Amount": 1}]}]==]
	},
	[12] = {
		["ID"] = 12,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100013, "Amount": 1}]}]==]
	},
	[13] = {
		["ID"] = 13,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100014, "Amount": 1}]}]==]
	},
	[14] = {
		["ID"] = 14,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100015, "Amount": 1}]}]==]
	},
	[15] = {
		["ID"] = 15,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100016, "Amount": 1}]}]==]
	},
	[16] = {
		["ID"] = 16,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200010, "Amount": 1}]}]==]
	},
	[17] = {
		["ID"] = 17,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200011, "Amount": 1}]}]==]
	},
	[18] = {
		["ID"] = 18,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200012, "Amount": 1}]}]==]
	},
	[19] = {
		["ID"] = 19,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200013, "Amount": 1}]}]==]
	},
	[20] = {
		["ID"] = 20,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200015, "Amount": 1}]}]==]
	},
	[21] = {
		["ID"] = 21,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100011, "Amount": 1}]}]==]
	},
	[22] = {
		["ID"] = 22,
		["LotteryId"] = 1,
		["Weight"] = 250,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200009, "Amount": 1}]}]==]
	},
	[23] = {
		["ID"] = 23,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200001, "Amount": 1}]}]==]
	},
	[24] = {
		["ID"] = 24,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200014, "Amount": 1}]}]==]
	},
	[25] = {
		["ID"] = 25,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100001, "Amount": 1}]}]==]
	},
	[26] = {
		["ID"] = 26,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100002, "Amount": 1}]}]==]
	},
	[27] = {
		["ID"] = 27,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100003, "Amount": 1}]}]==]
	},
	[28] = {
		["ID"] = 28,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100004, "Amount": 1}]}]==]
	},
	[29] = {
		["ID"] = 29,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100005, "Amount": 1}]}]==]
	},
	[30] = {
		["ID"] = 30,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100006, "Amount": 1}]}]==]
	},
	[31] = {
		["ID"] = 31,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100007, "Amount": 1}]}]==]
	},
	[32] = {
		["ID"] = 32,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100008, "Amount": 1}]}]==]
	},
	[33] = {
		["ID"] = 33,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100009, "Amount": 1}]}]==]
	},
	[34] = {
		["ID"] = 34,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 100010, "Amount": 1}]}]==]
	},
	[35] = {
		["ID"] = 35,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200002, "Amount": 1}]}]==]
	},
	[36] = {
		["ID"] = 36,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200003, "Amount": 1}]}]==]
	},
	[37] = {
		["ID"] = 37,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200004, "Amount": 1}]}]==]
	},
	[38] = {
		["ID"] = 38,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200005, "Amount": 1}]}]==]
	},
	[39] = {
		["ID"] = 39,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200006, "Amount": 1}]}]==]
	},
	[40] = {
		["ID"] = 40,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200007, "Amount": 1}]}]==]
	},
	[41] = {
		["ID"] = 41,
		["LotteryId"] = 1,
		["Weight"] = 100,
		["Prize"] = [==[{"Resources": [{"Type": "ITEM", "Id": 1, "Amount": 1}, {"Type": "HERO", "Id": 200008, "Amount": 1}]}]==]
	}
}