return {
	[1] = {
		["AchieveId"] = 1,
		["Name"] = "活跃度20",
		["Desc"] = "20.0",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 7, "Amount": 20}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==]
	},
	[2] = {
		["AchieveId"] = 2,
		["Name"] = "活跃度40",
		["Desc"] = "40.0",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 7, "Amount": 40}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==]
	},
	[3] = {
		["AchieveId"] = 3,
		["Name"] = "活跃度60",
		["Desc"] = "60.0",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 7, "Amount": 60}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==]
	},
	[4] = {
		["AchieveId"] = 4,
		["Name"] = "活跃度80",
		["Desc"] = "80.0",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 7, "Amount": 80}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==]
	},
	[5] = {
		["AchieveId"] = 5,
		["Name"] = "活跃度100",
		["Desc"] = "100.0",
		["Consume"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 7, "Amount": 100}]}]==],
		["Reward"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 50}]}]==]
	}
}