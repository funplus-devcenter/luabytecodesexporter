return {
	[1] = {
		["ID"] = 1,
		["Type"] = 1,
		["Name"] = "普通卡池",
		["Desc"] = "普通卡池",
		["AdvResource"] = "Lottery_1",
		["Order"] = 1,
		["FreeTime"] = 24,
		["Price"] = [==[{"Resources": [{"Type": "ITEM", "Id": 9, "Amount": 1}]}]==],
		["TenPrice"] = [==[{"Resources": [{"Type": "ITEM", "Id": 9, "Amount": 10}]}]==],
		["IsTenMustSSR"] = true,
		["MustSSRTimes"] = 0
	}
}