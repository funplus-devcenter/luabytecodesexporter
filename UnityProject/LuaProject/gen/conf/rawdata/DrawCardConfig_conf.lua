return {
	[1] = {
		["ID"] = 1,
		["Type"] = 1,
		["Name"] = "普通卡池",
		["Desc"] = "普通卡池",
		["AdvResource"] = "Null",
		["Order"] = 1,
		["FreeTime"] = 24,
		["Price"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}]}]==],
		["TenPrice"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}]}]==]
	},
	[2] = {
		["ID"] = 2,
		["Type"] = 2,
		["Name"] = "新手卡池",
		["Desc"] = "新手卡池",
		["AdvResource"] = "Null",
		["Order"] = 2,
		["FreeTime"] = 24,
		["Price"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}]}]==],
		["TenPrice"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}]}]==]
	},
	[3] = {
		["ID"] = 3,
		["Type"] = 3,
		["Name"] = "活动卡池",
		["Desc"] = "活动卡池",
		["AdvResource"] = "Null",
		["Order"] = 3,
		["FreeTime"] = 24,
		["Price"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}]}]==],
		["TenPrice"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 100}]}]==]
	}
}