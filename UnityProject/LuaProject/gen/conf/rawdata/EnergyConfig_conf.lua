return {
	[1] = {
		["Times"] = 1,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 10}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	},
	[2] = {
		["Times"] = 2,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 10}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	},
	[3] = {
		["Times"] = 3,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 10}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	},
	[4] = {
		["Times"] = 4,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 20}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	},
	[5] = {
		["Times"] = 5,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 20}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	},
	[6] = {
		["Times"] = 6,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 20}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	},
	[7] = {
		["Times"] = 7,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	},
	[8] = {
		["Times"] = 8,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	},
	[9] = {
		["Times"] = 9,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 30}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	},
	[10] = {
		["Times"] = 10,
		["Cost"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 3, "Amount": 40}]}]==],
		["Prize"] = [==[{"Resources": [{"Type": "CURRENCY", "Id": 4, "Amount": 50}]}]==]
	}
}