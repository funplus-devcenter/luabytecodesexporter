return {
	[1001] = {
		["ID"] = 1001,
		["Name"] = "物品1",
		["Skill"] = 4001,
		["Desc"] = "群体技能",
		["Icon"] = "im_fuhao02",
		["Limit"] = 10
	},
	[1002] = {
		["ID"] = 1002,
		["Name"] = "物品2",
		["Skill"] = 4001,
		["Desc"] = "单体技能",
		["Icon"] = "im_fuhao03",
		["Limit"] = 5
	},
	[1003] = {
		["ID"] = 1003,
		["Name"] = "物品3",
		["Skill"] = 4001,
		["Desc"] = "治疗技能",
		["Icon"] = "im_fuhao04",
		["Limit"] = 5
	},
	[1004] = {
		["ID"] = 1004,
		["Name"] = "物品4",
		["Skill"] = 4001,
		["Desc"] = "攻击力增加",
		["Icon"] = "im_fuhao05",
		["Limit"] = 5
	}
}