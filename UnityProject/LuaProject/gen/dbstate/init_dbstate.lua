local pb = require 'pb'

local CURRENT_MODULE_NAME = ...
dbstate = dbstate or {}
dbstate.PACKAGE_NAME = string.sub(CURRENT_MODULE_NAME, 1, -14)
function dbstate:initAll()
	 require (dbstate.PACKAGE_NAME .. ".hero_pb")
	 require (dbstate.PACKAGE_NAME .. ".mail_pb")
	 require (dbstate.PACKAGE_NAME .. ".player_pb")
end

