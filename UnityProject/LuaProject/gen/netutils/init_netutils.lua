local pb = require 'pb'

local CURRENT_MODULE_NAME = ...
netutils = netutils or {}
netutils.PACKAGE_NAME = string.sub(CURRENT_MODULE_NAME, 1, -15)
function netutils:initAll()
	 require (netutils.PACKAGE_NAME .. ".common_pb")
	 require (netutils.PACKAGE_NAME .. ".packet_pb")
	 netutils.metadata = require (netutils.PACKAGE_NAME .. ".metadata")
	 netutils.defaultMetadata = netutils.metadata.new()
end

netutils:initAll()

netutils.index = 0 
function netutils:genPassThrough()
	self.index = self.index + 1
	return os.time() .. '_' .. netutils.index
end

---@param msg table
---@param passThrough string
---@param md Metadata
---@return RawPacket,string actually is bytes
function netutils:marshalRawPacket(msg,passThrough,md)
	local any = netutils.RawAny:new()
	any.uri = msg:getMessageName()
	any.raw = msg:Marshal()
	any.passThrough = passThrough or self:genPassThrough()
	local packet = netutils.RawPacket:new()
	local mdLocal = netutils.metadata.join(netutils.defaultMetadata,md or netutils.metadata.new())
	mdLocal:set("x_create_time",os.time())
	packet.metadata = netutils.metadata.toPairs(mdLocal)
	packet.rawAny = any
	packet.SequenceID = netutils.index
	return packet,packet:Marshal()
end

---@param bytes string actually is bytes
---@return table
function netutils:unmarshalRawPacket(bytes)
	local packet = pb.decode(netutils.RawPacket:getMessageName(),bytes)
	return {uri=packet.rawAny.uri,sequenceID=packet.SequenceID,md=netutils.metadata.fromPairs(packet.metadata),message=pb.decode(packet.rawAny.uri,packet.rawAny.raw),passThrough=packet.rawAny.passThrough}
end
