local pb = require 'pb'

local CURRENT_MODULE_NAME = ...
service = service or {}
service.PACKAGE_NAME = string.sub(CURRENT_MODULE_NAME, 1, -14)
function service:initAll()
	 require (service.PACKAGE_NAME .. ".auth_service")
	 require (service.PACKAGE_NAME .. ".chapter_service")
	 require (service.PACKAGE_NAME .. ".cheat_command_service")
	 require (service.PACKAGE_NAME .. ".combat_service")
	 require (service.PACKAGE_NAME .. ".hero_service")
	 require (service.PACKAGE_NAME .. ".mail_service")
	 require (service.PACKAGE_NAME .. ".ping_service")
	 require (service.PACKAGE_NAME .. ".player_service")
	 require (service.PACKAGE_NAME .. ".shop_service")
	 require (service.PACKAGE_NAME .. ".story_service")
	 require (service.PACKAGE_NAME .. ".task_service")
end

