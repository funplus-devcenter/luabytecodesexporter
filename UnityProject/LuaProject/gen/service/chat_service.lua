-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: service/chat.proto

local pb = require "pb"
local protoc = require "protoc"
require (GEN_PACKAGE_NAME ..".msg.chat_pb")



---@class ChatService
local ChatService = {}
-- room chat
ChatService.Chat = {}
ChatService.Chat.uri = "/api/v1/chat"
ChatService.Chat.req = msg.Chat
ChatService.Chat.rsp = msg.Chat

service.ChatService = ChatService
