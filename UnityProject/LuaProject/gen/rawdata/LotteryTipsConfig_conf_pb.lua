-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/LotteryTipsConfig_conf.proto

local pb = require "pb"
local protoc = require "protoc"
require (GEN_PACKAGE_NAME ..".rawdata.LotteryTipsConfig_pb")

local protoStr = [==[
syntax = "proto3";
import "rawdata/LotteryTipsConfig.proto";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message LotteryTipsConfig_conf {
	map<int32,rawdata.LotteryTipsConfig> LotteryTipsConfigs =1;
}
]==]
assert(protoc:load(protoStr,"rawdata/LotteryTipsConfig_conf.proto"))


---@class LotteryTipsConfig_conf
local LotteryTipsConfig_conf = {}
---@type table<number, LotteryTipsConfig>
LotteryTipsConfig_conf.LotteryTipsConfigs = {}	-- map <int32, rawdata.LotteryTipsConfig>
---@return string
function LotteryTipsConfig_conf:getMessageName() return "rawdata.LotteryTipsConfig_conf" end
---@return LotteryTipsConfig_conf
function LotteryTipsConfig_conf:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function LotteryTipsConfig_conf:Marshal()  return pb.encode(self:getMessageName(),self) end
function LotteryTipsConfig_conf:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.LotteryTipsConfig_conf = LotteryTipsConfig_conf

---@class LotteryTipsConfig_conf_LotteryTipsConfigsEntry
local LotteryTipsConfig_conf_LotteryTipsConfigsEntry = {}
LotteryTipsConfig_conf_LotteryTipsConfigsEntry.key = 0
LotteryTipsConfig_conf_LotteryTipsConfigsEntry.value = {} -- type LotteryTipsConfig,use LotteryTipsConfig:new() instead?
---@return string
function LotteryTipsConfig_conf_LotteryTipsConfigsEntry:getMessageName() return "rawdata.LotteryTipsConfig_conf.LotteryTipsConfigsEntry" end
---@return LotteryTipsConfig_conf_LotteryTipsConfigsEntry
function LotteryTipsConfig_conf_LotteryTipsConfigsEntry:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function LotteryTipsConfig_conf_LotteryTipsConfigsEntry:Marshal()  return pb.encode(self:getMessageName(),self) end
function LotteryTipsConfig_conf_LotteryTipsConfigsEntry:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.LotteryTipsConfig_conf_LotteryTipsConfigsEntry = LotteryTipsConfig_conf_LotteryTipsConfigsEntry

