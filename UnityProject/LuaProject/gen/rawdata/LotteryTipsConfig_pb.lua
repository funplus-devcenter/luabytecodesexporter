-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/LotteryTipsConfig.proto

local pb = require "pb"
local protoc = require "protoc"

local protoStr = [==[
syntax = "proto3";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message LotteryTipsConfig {
	// 卡牌
	int32 Card = 1;
	// 是否显示概率翻倍，1为显示
	int32 Double = 2;
	// ID
	int32 ID = 3;
	// 抽卡id
	int32 LotteryId = 4;
}
]==]
assert(protoc:load(protoStr,"rawdata/LotteryTipsConfig.proto"))


---@class LotteryTipsConfig
local LotteryTipsConfig = {}
LotteryTipsConfig.Card = 0
LotteryTipsConfig.Double = 0
LotteryTipsConfig.ID = 0
LotteryTipsConfig.LotteryId = 0
---@return string
function LotteryTipsConfig:getMessageName() return "rawdata.LotteryTipsConfig" end
---@return LotteryTipsConfig
function LotteryTipsConfig:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function LotteryTipsConfig:Marshal()  return pb.encode(self:getMessageName(),self) end
function LotteryTipsConfig:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.LotteryTipsConfig = LotteryTipsConfig

