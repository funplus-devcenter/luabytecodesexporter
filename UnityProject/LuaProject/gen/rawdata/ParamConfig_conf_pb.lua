-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/ParamConfig_conf.proto

local pb = require "pb"
local protoc = require "protoc"
require (GEN_PACKAGE_NAME ..".rawdata.ParamConfig_pb")

local protoStr = [==[
syntax = "proto3";
import "rawdata/ParamConfig.proto";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message ParamConfig_conf {
	map<uint32,rawdata.ParamConfig> ParamConfigs =1;
}
]==]
assert(protoc:load(protoStr,"rawdata/ParamConfig_conf.proto"))


---@class ParamConfig_conf
local ParamConfig_conf = {}
---@type table<number, ParamConfig>
ParamConfig_conf.ParamConfigs = {}	-- map <uint32, rawdata.ParamConfig>
---@return string
function ParamConfig_conf:getMessageName() return "rawdata.ParamConfig_conf" end
---@return ParamConfig_conf
function ParamConfig_conf:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function ParamConfig_conf:Marshal()  return pb.encode(self:getMessageName(),self) end
function ParamConfig_conf:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.ParamConfig_conf = ParamConfig_conf

---@class ParamConfig_conf_ParamConfigsEntry
local ParamConfig_conf_ParamConfigsEntry = {}
ParamConfig_conf_ParamConfigsEntry.key = 0
ParamConfig_conf_ParamConfigsEntry.value = {} -- type ParamConfig,use ParamConfig:new() instead?
---@return string
function ParamConfig_conf_ParamConfigsEntry:getMessageName() return "rawdata.ParamConfig_conf.ParamConfigsEntry" end
---@return ParamConfig_conf_ParamConfigsEntry
function ParamConfig_conf_ParamConfigsEntry:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function ParamConfig_conf_ParamConfigsEntry:Marshal()  return pb.encode(self:getMessageName(),self) end
function ParamConfig_conf_ParamConfigsEntry:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.ParamConfig_conf_ParamConfigsEntry = ParamConfig_conf_ParamConfigsEntry

