-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/ChapterStarRewardConfig.proto

local pb = require "pb"
local protoc = require "protoc"

local protoStr = [==[
syntax = "proto3";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message ChapterStarRewardConfig {
	// 所属章节
	int32 ChapterID = 1;
	// ID
	int32 ID = 2;
	// 名称
	string Name = 3;
	// 所需星星数量
	int32 NeedStar = 4;
	// 固定奖励
	string Reward = 5;
}
]==]
assert(protoc:load(protoStr,"rawdata/ChapterStarRewardConfig.proto"))


---@class ChapterStarRewardConfig
local ChapterStarRewardConfig = {}
ChapterStarRewardConfig.ChapterID = 0
ChapterStarRewardConfig.ID = 0
ChapterStarRewardConfig.Name = ""
ChapterStarRewardConfig.NeedStar = 0
ChapterStarRewardConfig.Reward = ""
---@return string
function ChapterStarRewardConfig:getMessageName() return "rawdata.ChapterStarRewardConfig" end
---@return ChapterStarRewardConfig
function ChapterStarRewardConfig:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function ChapterStarRewardConfig:Marshal()  return pb.encode(self:getMessageName(),self) end
function ChapterStarRewardConfig:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.ChapterStarRewardConfig = ChapterStarRewardConfig

