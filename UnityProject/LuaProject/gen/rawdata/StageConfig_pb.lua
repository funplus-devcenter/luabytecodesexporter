-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/StageConfig.proto

local pb = require "pb"
local protoc = require "protoc"

local protoStr = [==[
syntax = "proto3";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message StageConfig {
	// ID
	uint32 ID = 1;
	// 怪物ID
	message MonsterIDComplexValue {
		 repeated uint32 value = 1;
	}
	repeated MonsterIDComplexValue MonsterID = 2;
	// 名称
	string Name = 3;
	// 怪物位置
	string PositionID = 4;
	// 资源
	string Res = 5;
	// 波次
	int32 Wave = 6;
}
]==]
assert(protoc:load(protoStr,"rawdata/StageConfig.proto"))


---@class StageConfig
local StageConfig = {}
StageConfig.ID = 0
---@type StageConfig_MonsterIDComplexValue[]
StageConfig.MonsterID = {}	-- repeated StageConfig_MonsterIDComplexValue
StageConfig.Name = ""
StageConfig.PositionID = ""
StageConfig.Res = ""
StageConfig.Wave = 0
---@return string
function StageConfig:getMessageName() return "rawdata.StageConfig" end
---@return StageConfig
function StageConfig:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function StageConfig:Marshal()  return pb.encode(self:getMessageName(),self) end
function StageConfig:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.StageConfig = StageConfig

---@class StageConfig_MonsterIDComplexValue
local StageConfig_MonsterIDComplexValue = {}
---@type number[]
StageConfig_MonsterIDComplexValue.value = {}	-- repeated uint32
---@return string
function StageConfig_MonsterIDComplexValue:getMessageName() return "rawdata.StageConfig.MonsterIDComplexValue" end
---@return StageConfig_MonsterIDComplexValue
function StageConfig_MonsterIDComplexValue:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function StageConfig_MonsterIDComplexValue:Marshal()  return pb.encode(self:getMessageName(),self) end
function StageConfig_MonsterIDComplexValue:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.StageConfig_MonsterIDComplexValue = StageConfig_MonsterIDComplexValue

