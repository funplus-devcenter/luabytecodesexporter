-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/LevelConfig.proto

local pb = require "pb"
local protoc = require "protoc"

local protoStr = [==[
syntax = "proto3";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message LevelConfig {
	// 体力上限
	int32 Energy = 1;
	// 经验值
	int32 Exp = 2;
	// ID
	int32 Level = 3;
	// 奖励
	string Reward = 4;
}
]==]
assert(protoc:load(protoStr,"rawdata/LevelConfig.proto"))


---@class LevelConfig
local LevelConfig = {}
LevelConfig.Energy = 0
LevelConfig.Exp = 0
LevelConfig.Level = 0
LevelConfig.Reward = ""
---@return string
function LevelConfig:getMessageName() return "rawdata.LevelConfig" end
---@return LevelConfig
function LevelConfig:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function LevelConfig:Marshal()  return pb.encode(self:getMessageName(),self) end
function LevelConfig:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.LevelConfig = LevelConfig

