-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/ParaConfig_conf.proto

local pb = require "pb"
local protoc = require "protoc"
require (GEN_PACKAGE_NAME ..".rawdata.ParaConfig_pb")

local protoStr = [==[
syntax = "proto3";
import "rawdata/ParaConfig.proto";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message ParaConfig_conf {
	map<uint32,rawdata.ParaConfig> ParaConfigs =1;
}
]==]
assert(protoc:load(protoStr,"rawdata/ParaConfig_conf.proto"))


---@class ParaConfig_conf
local ParaConfig_conf = {}
---@type table<number, ParaConfig>
ParaConfig_conf.ParaConfigs = {}	-- map <uint32, rawdata.ParaConfig>
---@return string
function ParaConfig_conf:getMessageName() return "rawdata.ParaConfig_conf" end
---@return ParaConfig_conf
function ParaConfig_conf:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function ParaConfig_conf:Marshal()  return pb.encode(self:getMessageName(),self) end
function ParaConfig_conf:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.ParaConfig_conf = ParaConfig_conf

---@class ParaConfig_conf_ParaConfigsEntry
local ParaConfig_conf_ParaConfigsEntry = {}
ParaConfig_conf_ParaConfigsEntry.key = 0
ParaConfig_conf_ParaConfigsEntry.value = {} -- type ParaConfig,use ParaConfig:new() instead?
---@return string
function ParaConfig_conf_ParaConfigsEntry:getMessageName() return "rawdata.ParaConfig_conf.ParaConfigsEntry" end
---@return ParaConfig_conf_ParaConfigsEntry
function ParaConfig_conf_ParaConfigsEntry:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function ParaConfig_conf_ParaConfigsEntry:Marshal()  return pb.encode(self:getMessageName(),self) end
function ParaConfig_conf_ParaConfigsEntry:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.ParaConfig_conf_ParaConfigsEntry = ParaConfig_conf_ParaConfigsEntry

