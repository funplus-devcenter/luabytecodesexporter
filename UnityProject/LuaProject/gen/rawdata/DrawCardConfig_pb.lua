-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/DrawCardConfig.proto

local pb = require "pb"
local protoc = require "protoc"

local protoStr = [==[
syntax = "proto3";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message DrawCardConfig {
	// 广告资源
	string AdvResource = 1;
	// 描述
	string Desc = 2;
	// 免费单抽间隔小时
	int32 FreeTime = 3;
	// ID
	int32 ID = 4;
	// 名称
	string Name = 5;
	// 排序字段
	int32 Order = 6;
	// 单抽消耗
	string Price = 7;
	// 十连抽消耗
	string TenPrice = 8;
	// 抽卡类型
	int32 Type = 9;
}
]==]
assert(protoc:load(protoStr,"rawdata/DrawCardConfig.proto"))


---@class DrawCardConfig
local DrawCardConfig = {}
DrawCardConfig.AdvResource = ""
DrawCardConfig.Desc = ""
DrawCardConfig.FreeTime = 0
DrawCardConfig.ID = 0
DrawCardConfig.Name = ""
DrawCardConfig.Order = 0
DrawCardConfig.Price = ""
DrawCardConfig.TenPrice = ""
DrawCardConfig.Type = 0
---@return string
function DrawCardConfig:getMessageName() return "rawdata.DrawCardConfig" end
---@return DrawCardConfig
function DrawCardConfig:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function DrawCardConfig:Marshal()  return pb.encode(self:getMessageName(),self) end
function DrawCardConfig:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.DrawCardConfig = DrawCardConfig

