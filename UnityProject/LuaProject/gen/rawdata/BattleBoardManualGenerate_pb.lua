-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/BattleBoardManualGenerate.proto

local pb = require "pb"
local protoc = require "protoc"

local protoStr = [==[
syntax = "proto3";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message BattleBoardManualGenerate {
	// 棋盘瓦片初始颜色(第一维为x，第二维为y)
	message BoardColorComplexValue {
		 repeated uint32 value = 1;
	}
	repeated BoardColorComplexValue BoardColor = 1;
	// ID
	int32 ID = 2;
	// 棋盘瓦片初始类型(第一维为x，第二维为y)
	message TileTypeComplexValue {
		 repeated uint32 value = 1;
	}
	repeated TileTypeComplexValue TileType = 6;
}
]==]
assert(protoc:load(protoStr,"rawdata/BattleBoardManualGenerate.proto"))


---@class BattleBoardManualGenerate
local BattleBoardManualGenerate = {}
---@type BattleBoardManualGenerate_BoardColorComplexValue[]
BattleBoardManualGenerate.BoardColor = {}	-- repeated BattleBoardManualGenerate_BoardColorComplexValue
BattleBoardManualGenerate.ID = 0
---@type BattleBoardManualGenerate_TileTypeComplexValue[]
BattleBoardManualGenerate.TileType = {}	-- repeated BattleBoardManualGenerate_TileTypeComplexValue
---@return string
function BattleBoardManualGenerate:getMessageName() return "rawdata.BattleBoardManualGenerate" end
---@return BattleBoardManualGenerate
function BattleBoardManualGenerate:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function BattleBoardManualGenerate:Marshal()  return pb.encode(self:getMessageName(),self) end
function BattleBoardManualGenerate:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.BattleBoardManualGenerate = BattleBoardManualGenerate

---@class BattleBoardManualGenerate_BoardColorComplexValue
local BattleBoardManualGenerate_BoardColorComplexValue = {}
---@type number[]
BattleBoardManualGenerate_BoardColorComplexValue.value = {}	-- repeated uint32
---@return string
function BattleBoardManualGenerate_BoardColorComplexValue:getMessageName() return "rawdata.BattleBoardManualGenerate.BoardColorComplexValue" end
---@return BattleBoardManualGenerate_BoardColorComplexValue
function BattleBoardManualGenerate_BoardColorComplexValue:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function BattleBoardManualGenerate_BoardColorComplexValue:Marshal()  return pb.encode(self:getMessageName(),self) end
function BattleBoardManualGenerate_BoardColorComplexValue:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.BattleBoardManualGenerate_BoardColorComplexValue = BattleBoardManualGenerate_BoardColorComplexValue

---@class BattleBoardManualGenerate_TileTypeComplexValue
local BattleBoardManualGenerate_TileTypeComplexValue = {}
---@type number[]
BattleBoardManualGenerate_TileTypeComplexValue.value = {}	-- repeated uint32
---@return string
function BattleBoardManualGenerate_TileTypeComplexValue:getMessageName() return "rawdata.BattleBoardManualGenerate.TileTypeComplexValue" end
---@return BattleBoardManualGenerate_TileTypeComplexValue
function BattleBoardManualGenerate_TileTypeComplexValue:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function BattleBoardManualGenerate_TileTypeComplexValue:Marshal()  return pb.encode(self:getMessageName(),self) end
function BattleBoardManualGenerate_TileTypeComplexValue:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.BattleBoardManualGenerate_TileTypeComplexValue = BattleBoardManualGenerate_TileTypeComplexValue

