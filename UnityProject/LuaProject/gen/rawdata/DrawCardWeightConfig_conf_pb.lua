-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/DrawCardWeightConfig_conf.proto

local pb = require "pb"
local protoc = require "protoc"
require (GEN_PACKAGE_NAME ..".rawdata.DrawCardWeightConfig_pb")

local protoStr = [==[
syntax = "proto3";
import "rawdata/DrawCardWeightConfig.proto";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message DrawCardWeightConfig_conf {
	map<int32,rawdata.DrawCardWeightConfig> DrawCardWeightConfigs =1;
}
]==]
assert(protoc:load(protoStr,"rawdata/DrawCardWeightConfig_conf.proto"))


---@class DrawCardWeightConfig_conf
local DrawCardWeightConfig_conf = {}
---@type table<number, DrawCardWeightConfig>
DrawCardWeightConfig_conf.DrawCardWeightConfigs = {}	-- map <int32, rawdata.DrawCardWeightConfig>
---@return string
function DrawCardWeightConfig_conf:getMessageName() return "rawdata.DrawCardWeightConfig_conf" end
---@return DrawCardWeightConfig_conf
function DrawCardWeightConfig_conf:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function DrawCardWeightConfig_conf:Marshal()  return pb.encode(self:getMessageName(),self) end
function DrawCardWeightConfig_conf:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.DrawCardWeightConfig_conf = DrawCardWeightConfig_conf

---@class DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry
local DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry = {}
DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry.key = 0
DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry.value = {} -- type DrawCardWeightConfig,use DrawCardWeightConfig:new() instead?
---@return string
function DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry:getMessageName() return "rawdata.DrawCardWeightConfig_conf.DrawCardWeightConfigsEntry" end
---@return DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry
function DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry:Marshal()  return pb.encode(self:getMessageName(),self) end
function DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry = DrawCardWeightConfig_conf_DrawCardWeightConfigsEntry

