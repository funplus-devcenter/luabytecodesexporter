-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: rawdata/ChapterStarConditionConfig.proto

local pb = require "pb"
local protoc = require "protoc"

local protoStr = [==[
syntax = "proto3";
package rawdata;
option go_package = "server/pkg/gen/rawdata";
option csharp_namespace = "rawdata";
message ChapterStarConditionConfig {
	// 描述
	string Desc = 1;
	// ID
	int32 ID = 2;
	// 参数
	string Params = 3;
	// 类型
	int32 Type = 4;
}
]==]
assert(protoc:load(protoStr,"rawdata/ChapterStarConditionConfig.proto"))


---@class ChapterStarConditionConfig
local ChapterStarConditionConfig = {}
ChapterStarConditionConfig.Desc = ""
ChapterStarConditionConfig.ID = 0
ChapterStarConditionConfig.Params = ""
ChapterStarConditionConfig.Type = 0
---@return string
function ChapterStarConditionConfig:getMessageName() return "rawdata.ChapterStarConditionConfig" end
---@return ChapterStarConditionConfig
function ChapterStarConditionConfig:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function ChapterStarConditionConfig:Marshal()  return pb.encode(self:getMessageName(),self) end
function ChapterStarConditionConfig:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
rawdata.ChapterStarConditionConfig = ChapterStarConditionConfig

