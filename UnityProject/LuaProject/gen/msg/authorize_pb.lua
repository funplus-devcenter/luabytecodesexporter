-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: msg/authorize.proto

local pb = require "pb"
local protoc = require "protoc"

local protoStr = [==[
syntax = "proto3";
package msg;
option go_package = "server/pkg/gen/msg";
message AuthorizeInput {
    string AccessToken = 1; // 平台token
    string Uid = 2; // 只有在debug模式下才会读取
    bytes ClientPublicKey = 3; // 用于签名计算的public-key
}
message AuthorizeOutput {
    bool IsSuccess = 1; // 验证是否成功
    string GameAccessToken = 2; // 游戏token
    int32 SequenceId = 3; // 请求序列号
}
]==]
assert(protoc:load(protoStr,"msg/authorize.proto"))


---@class AuthorizeInput
local AuthorizeInput = {}
AuthorizeInput.AccessToken = ""
AuthorizeInput.Uid = ""
AuthorizeInput.ClientPublicKey = 0
---@return string
function AuthorizeInput:getMessageName() return "msg.AuthorizeInput" end
---@return AuthorizeInput
function AuthorizeInput:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function AuthorizeInput:Marshal()  return pb.encode(self:getMessageName(),self) end
function AuthorizeInput:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
msg.AuthorizeInput = AuthorizeInput

---@class AuthorizeOutput
local AuthorizeOutput = {}
AuthorizeOutput.IsSuccess = false
AuthorizeOutput.GameAccessToken = ""
AuthorizeOutput.SequenceId = 0
---@return string
function AuthorizeOutput:getMessageName() return "msg.AuthorizeOutput" end
---@return AuthorizeOutput
function AuthorizeOutput:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function AuthorizeOutput:Marshal()  return pb.encode(self:getMessageName(),self) end
function AuthorizeOutput:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
msg.AuthorizeOutput = AuthorizeOutput

