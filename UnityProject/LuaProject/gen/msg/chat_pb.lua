-- Code generated by protoc-gen-lua. DO NOT EDIT.
-- source: msg/chat.proto

local pb = require "pb"
local protoc = require "protoc"

local protoStr = [==[
syntax = "proto3";
package msg;
option go_package = "server/pkg/gen/msg";
message Chat {
    uint64 PlayerID = 100;
    string text = 1;
    repeated int32 test =2;
}
]==]
assert(protoc:load(protoStr,"msg/chat.proto"))


---@class Chat
local Chat = {}
Chat.PlayerID = 0
Chat.text = ""
---@type number[]
Chat.test = {}	-- repeated int32
---@return string
function Chat:getMessageName() return "msg.Chat" end
---@return Chat
function Chat:new() local o = {} setmetatable(o, self) self.__index = self return o  end
function Chat:Marshal()  return pb.encode(self:getMessageName(),self) end
function Chat:Unmarshal(bytes)  return pb.decode(self:getMessageName(),bytes) end
msg.Chat = Chat

