local pb = require 'pb'

local CURRENT_MODULE_NAME = ...
msg = msg or {}
msg.PACKAGE_NAME = string.sub(CURRENT_MODULE_NAME, 1, -10)
function msg:initAll()
	 require (msg.PACKAGE_NAME .. ".authorize_pb")
	 require (msg.PACKAGE_NAME .. ".combat_pb")
	 require (msg.PACKAGE_NAME .. ".resource_pb")
	 require (msg.PACKAGE_NAME .. ".chapter_pb")
	 require (msg.PACKAGE_NAME .. ".cheat_command_pb")
	 require (msg.PACKAGE_NAME .. ".common_pb")
	 require (msg.PACKAGE_NAME .. ".currency_pb")
	 require (msg.PACKAGE_NAME .. ".error_pb")
	 require (msg.PACKAGE_NAME .. ".item_pb")
	 require (msg.PACKAGE_NAME .. ".team_pb")
	 require (msg.PACKAGE_NAME .. ".hero_pb")
	 require (msg.PACKAGE_NAME .. ".lottery_pb")
	 require (msg.PACKAGE_NAME .. ".mail_pb")
	 require (msg.PACKAGE_NAME .. ".task_pb")
	 require (msg.PACKAGE_NAME .. ".ping_pb")
	 require (msg.PACKAGE_NAME .. ".shop_pb")
	 require (msg.PACKAGE_NAME .. ".player_pb")
	 require (msg.PACKAGE_NAME .. ".story_pb")
end

